package tvdb;

import java.sql.Connection;

import dao.DAO_Management;
import data.Data_Agenda;
import data.Data_Category;
import exceptions.Exce_DAO_Exception;
import inter.Inter_TVDB;
import tools.Tool_Treatment;
import tools.Tool_Validation;
import tvdb.rules.Rules_Agenda;

/**
 *
 * Archivo TVDB_Agenda.java
 * @author Joaquin Reyes Sanchez [Hakyn Seyer] <joaquin.seyer21@gmail.com>
 * Creado el día 28/01/19 a las 11:57 PM
 * @license This software is MIT licensed (see LICENSE)
 * 
*/

public abstract class TVDB_Agenda implements Inter_TVDB<Data_Agenda> {
	
	private Data_Agenda newAgenda, oldAgenda;
	
	private DAO_Management manaDB = null;
	
	/**
	 * 
	 * @param conn Conexion a la base de datos
	 * @param newAgenda Agenda para un nuevo registro
	 * @param oldAgenda Agenda existente en la base de datos
	 */
	public TVDB_Agenda (Connection conn, Data_Agenda newAgenda, Data_Agenda oldAgenda) {
		this.manaDB = DAO_Management.init(conn);
		
		this.newAgenda = newAgenda;
		this.oldAgenda = oldAgenda;
	}
	
	@Override
	public void Treatment() {
		// Web
		String web = this.newAgenda.getWeb();
		web = Tool_Treatment.trimTextAccents(web, Rules_Agenda.Treat_textWeb);
		this.newAgenda.setWeb(web);
		
		// Alias
		String alias = this.newAgenda.getAlias();
		alias = Tool_Treatment.trim(alias);
		this.newAgenda.setAlias(alias);
		
		// Email
		String email = this.newAgenda.getEmail();
		email = Tool_Treatment.trim(email);
		this.newAgenda.setEmail(email);
			
		// Password
		String password = this.newAgenda.getPassword();
		password = Tool_Treatment.trim(password);
		this.newAgenda.setPassword(password);
			
		// Details
		String details = this.newAgenda.getDetails();
		details = Tool_Treatment.trim(details);
		details = Tool_Treatment.text(details, Rules_Agenda.Treat_textDetails);
		this.newAgenda.setDetails(details);
	}

	@Override
	public boolean Validate() {
		Tool_Validation<Data_Agenda> val = new Tool_Validation<Data_Agenda>() {

			@Override
			public String noEqualObject(Data_Agenda original, Data_Agenda compare) {
				// TODO Auto-generated method stub
				return null;
			}
			
		};
		
		int successVal = 0;
		
		// Web
		String errorWeb = val.valEmptyMinMax(
				this.newAgenda.getWeb(),
				Rules_Agenda.Val_minWeb,
				Rules_Agenda.Val_maxWeb);
		
		if (!errorWeb.equals(""))
			this.noticeWeb(errorWeb);
		else {
			successVal++;
			this.clearNoticeWeb();
		};
		
		// Alias
		String errorAlias = val.valOptionalEmptyMinMax(
				this.newAgenda.getAlias(), 
				Rules_Agenda.Val_minAlias,
				Rules_Agenda.Val_maxAlias);
		
		if (!errorAlias.equals(""))
			this.noticeAlias(errorAlias);
		else {
			successVal++;
			this.clearNoticeAlias();
		};
		
		// Email
		String errorEmail = val.valEmptyMinMax(
				this.newAgenda.getEmail(), 
				Rules_Agenda.Val_minEmail,
				Rules_Agenda.Val_maxEmail);
		
		if (!errorEmail.equals(""))
			this.noticeEmail(errorEmail);
		else {
			successVal++;
			this.clearNoticeEmail();
		};
		
		// Password
		String errorPassword = val.valEmptyMinMax(
				this.newAgenda.getPassword(), 
				Rules_Agenda.Val_minPassword,
				Rules_Agenda.Val_maxPassword);
		
		if (!errorPassword.equals(""))
			this.noticePassword(errorPassword);
		else {
			successVal++;
			this.clearNoticePassword();
		};
		
		// Details
		String errorDetails = val.valOptionalEmptyMinMax(
				this.newAgenda.getDetails(),
				Rules_Agenda.Val_minDetails,
				Rules_Agenda.Val_maxDetails);
		
		if (!errorDetails.equals(""))
			this.noticeDetails(errorDetails);
		else {
			successVal++;
			this.clearNoticeDetails();
		};
		
		if (successVal == 5) return true;
	
		return false;
	}
	
	@Override
	public boolean ValidateUpdate() {
		Tool_Validation<Data_Agenda> val = new Tool_Validation<Data_Agenda>() {

			@Override
			public String noEqualObject(Data_Agenda original, Data_Agenda compare) {
				int successVal = 0;
							
				if (this.noEqual(original.getWeb(), compare.getWeb(), "Web").equals("")) successVal++;
				if (this.noEqual(original.getAlias(), compare.getAlias(), "Alias").equals("")) successVal++;
				if (this.noEqual(original.getEmail(), compare.getEmail(), "Email").equals("")) successVal++;
				if (this.noEqual(original.getPassword(), compare.getPassword(), "Password").equals("")) successVal++;
				if (this.noEqual(original.getCategory(), compare.getDataCategory().getId(), "Category").equals("")) successVal++;
				if (this.noEqual(original.getDetails(), compare.getDetails(), "Details").equals("")) successVal++;
				
				if (successVal > 0) return "";
				else return this.getError("valNoEqualObject");
			}
			
		};
		
		String errorAgenda = val.noEqualObject(this.newAgenda, this.oldAgenda);
		
		if (!errorAgenda.equals("")) {
			this.noticeNoChanges(errorAgenda);
			
			return false;
		} else this.clearNoticeNoChanges();
		
		return true;
	}
	
	@Override
	public boolean ValidateDataBase(int statusForm) throws Exce_DAO_Exception {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean DataBase() throws Exce_DAO_Exception {
		return manaDB.daoAgenda().create(this.newAgenda);		
	}
	
	@Override
	public boolean DataBaseUpdate() throws Exce_DAO_Exception {
		Data_Agenda updateAgenda = this.newAgenda;
		updateAgenda.setId(this.oldAgenda.getId());
		
		return this.manaDB.daoAgenda().update(updateAgenda); 
	}
	
	public boolean DataBaseTransactionCreate (Data_Category category) throws Exce_DAO_Exception {
		return this.manaDB.daoCategoryAgenda().transactionCreate(category, this.newAgenda);
	}
	
	public boolean DataBaseTransactionUpdate (Data_Category category) throws Exce_DAO_Exception {
		Data_Agenda updateAgenda = this.newAgenda;
		updateAgenda.setId(this.oldAgenda.getId());
		
		return this.manaDB.daoCategoryAgenda().transactionUpdate(category, updateAgenda);
	}
	
	@Override
	public boolean DataBaseDelete() throws Exce_DAO_Exception {
		return this.manaDB.daoAgenda().delete(this.oldAgenda);
	}

	@Override
	public Data_Agenda getNewData() {
		return this.newAgenda;
	}
	
	public abstract void noticeNoChanges(String error);
	public abstract void clearNoticeNoChanges();
	
	public abstract void noticeWeb(String error);
	public abstract void clearNoticeWeb();
	
	public abstract void noticeAlias(String error);
	public abstract void clearNoticeAlias();
	
	public abstract void noticeEmail(String error);
	public abstract void clearNoticeEmail();
	
	public abstract void noticePassword(String error);
	public abstract void clearNoticePassword();
	
	public abstract void noticeDetails (String error);
	public abstract void clearNoticeDetails();

}
