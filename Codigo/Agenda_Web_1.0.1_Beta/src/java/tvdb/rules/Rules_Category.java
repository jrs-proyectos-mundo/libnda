package tvdb.rules;

/**
 *
 * Archivo Rules_Category.java
 * @author Joaquin Reyes Sanchez [Hakyn Seyer] <joaquin.seyer21@gmail.com>
 * Creado el día 28/01/19 a las 11:56 PM
 * @license This software is MIT licensed (see LICENSE)
 * 
*/

public interface Rules_Category {
//	public int emptySelect = 0;
	
	public String Treat_textName = "capitalizeFully";
	public int Val_minName = 5;
	public int Val_maxName = 50;
}
