package tvdb.rules;

/**
 *
 * Archivo Rules_Agenda.java
 * @author Joaquin Reyes Sanchez [Hakyn Seyer] <joaquin.seyer21@gmail.com>
 * Creado el día 28/01/19 a las 11:56 PM
 * @license This software is MIT licensed (see LICENSE)
 * 
*/

public interface Rules_Agenda {
	public String Treat_textWeb = "capitalizeFully";
	public int Val_minWeb = 3;
	public int Val_maxWeb = 50;
	
	public int Val_minAlias = 4;
	public int Val_maxAlias = 50;
	
	public int Val_minEmail = 4;
	public int Val_maxEmail = 100;
	
	public int Val_minPassword = 4;
	public int Val_maxPassword = 100;
	
	public String Treat_textDetails = "lowercase";
	public int Val_minDetails = 4;
	public int Val_maxDetails = 255;

}
