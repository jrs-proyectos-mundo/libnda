package data;

/**
 *
 * Archivo Data_User.java
 * @author Joaquin Reyes Sanchez [Hakyn Seyer] <joaquin.seyer21@gmail.com>
 * Creado el día 28/01/19 a las 11:50 PM
 * @license This software is MIT licensed (see LICENSE)
 * 
*/

public class Data_User {

  private int id;
  private String name;
  private String surnames;

  private Data_User (int id, String name, String surnames) {
    this.id = id;
    this.name = name;
    this.surnames = surnames;
  }

  /**
   * @return the id
   */
  public int getId() {
    return id;
  }
  /**
   * @param id the id to set
   */
  public void setId(int id) {
    this.id = id;
  }

  /**
   * @return the name
   */
  public String getName() {
    return name;
  }
  /**
   * @param name the name to set
   */
  public void setName(String name) {
    this.name = name;
  }

  /**
   * @return the surnames
   */
  public String getSurnames() {
    return surnames;
  }
  /**
   * @param surnames the surnames to set
   */
  public void setSurnames(String surnames) {
    this.surnames = surnames;
  }

}