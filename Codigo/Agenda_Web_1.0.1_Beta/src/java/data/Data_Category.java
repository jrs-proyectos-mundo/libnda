package data;

/**
 *
 * Archivo Data_Category.java
 * @author Joaquin Reyes Sanchez [Hakyn Seyer] <joaquin.seyer21@gmail.com>
 * Creado el día 28/01/19 a las 11:50 PM
 * @license This software is MIT licensed (see LICENSE)
 * 
*/

public class Data_Category {

  private int id = -1;
  private String name = null;
  
  public Data_Category (int id, String name) {
    this.id = id;
    this.name = name;
  }
  
  public Data_Category () {}

  /**
   * @return the id
   */
  public int getId() {
    return id;
  }
  /**
   * @param id the id to set
   */
  public void setId(int id) {
    this.id = id;
  }

  /**
   * @return the name
   */
  public String getName() {
    return name;
  }
  /**
   * @param name the name to set
   */
  public void setName(String name) {
    this.name = name;
  }

  @Override
  public String toString() {
    return "" +
      "==============\n" +
      "Category = " + this.id + " :\n" +
      "    Name = " + this.name + "\n" +
      "==============\n";
  }

}