package exceptions;

/**
 *
 * Archivo Exce_DAO_Exception.java
 * @author Joaquin Reyes Sanchez [Hakyn Seyer] <joaquin.seyer21@gmail.com>
 * Creado el día 28/01/19 a las 11:51 PM
 * @license This software is MIT licensed (see LICENSE)
 * 
*/

@SuppressWarnings("serial")
public class Exce_DAO_Exception extends Exception {

	public Exce_DAO_Exception () {
		super();
	}
	
	public Exce_DAO_Exception(String message) {
		super(message);
	}
	
	public Exce_DAO_Exception(String message, Throwable cause) {
		super(message, cause);
	}
	
	public Exce_DAO_Exception(Throwable cause) {
		super(cause);
	}
	
}
