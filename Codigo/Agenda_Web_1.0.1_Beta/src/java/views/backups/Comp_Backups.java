package views.backups;

import javafx.scene.layout.VBox;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;

/**
 *
 * Archivo Comp_Backups.java
 * @author Joaquin Reyes Sanchez [Hakyn Seyer] <joaquin.seyer21@gmail.com>
 * Creado el día 28/01/19 a las 11:58 PM
 * @license This software is MIT licensed (see LICENSE)
 * 
*/

public class Comp_Backups {
	
	// Scrolls
	public final ScrollPane scroll_content = new ScrollPane();
	
	// Main Layout
	public final VBox layout_main = new VBox();
	public final VBox layout_content = new VBox();
	public final VBox layout_control = new VBox();
	
	// Validaciones
	public final Label val_db = new Label();
	
	// Área de Descripciones
	public final Label backup_description = new Label();
	public final Label restore_description = new Label();
	
	// Área de Botones
	public final Button btn_backup = new Button();
	public final Button btn_restore = new Button();
	public final Button btn_exit = new Button();
	
	
	private static Comp_Backups compBakcups = null;
	
	public static Comp_Backups init () {
		if (Comp_Backups.compBakcups == null)
			Comp_Backups.compBakcups = new Comp_Backups();
		
		return Comp_Backups.compBakcups;
	}
	
	private Comp_Backups () {}
}
