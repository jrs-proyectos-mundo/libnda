package views.backups.mixins;

import java.io.*;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;

import exceptions.Exce_DAO_Exception;
import inter.Inter_DB;
import javafx.scene.control.Control;
import javafx.stage.DirectoryChooser;
import javafx.stage.FileChooser;
import mixins.Mix_Components;
import mixins.Mix_Lang;
import tools.Tool_ConnectionDB;
import tools.Tool_ErrorsBackRestore;
import tools.Tool_ReqJSON;
import tools.Tool_Text;
import views.View_Backups;
import views.View_Home;
import views.backups.Comp_Backups;
import views.category.mixins.Mix_FormCategory;
import views.home.mixins.Mix_Board;
import views.registry.mixins.Mix_FormAgenda;

/**
 *
 * Archivo Mix_Menu.java
 * @author Joaquin Reyes Sanchez [Hakyn Seyer] <joaquin.seyer21@gmail.com>
 * Creado el día 28/01/19 a las 11:57 PM
 * @license This software is MIT licensed (see LICENSE)
 * 
*/

public class Mix_Menu {
	
	public static void clearValidator () {
		Comp_Backups compBacks = Comp_Backups.init();
		
		compBacks.val_db.setMinHeight(0);
		compBacks.val_db.setText("");
		compBacks.val_db.getStyleClass().clear();
		compBacks.val_db.getStyleClass().addAll("validator", "validator--main");
		compBacks.val_db.setVisible(false);
	}

	public static void closeWindow () {
		View_Backups winBack = View_Backups.init();
		Mix_Board.enableDisableControls(false);
		winBack.getWindow().close();
	}
	
	/**
	 * 
	 * @param status True para desactivar. False para activar
	 */
	public static void enableDisableElements (boolean status) {
		Comp_Backups compBacks = Comp_Backups.init();
		
		if (compBacks.btn_backup.isDisable() != status)
			compBacks.btn_backup.setDisable(status);
		if (compBacks.btn_restore.isDisable() != status)
			compBacks.btn_restore.setDisable(status);
		
	}
	
	public static void backupDB () {
		Mix_Menu.clearValidator();
		Mix_Menu.enableDisableElements(true);
		
		Comp_Backups compBacks = Comp_Backups.init();
		compBacks.val_db.setText(Mix_Lang.getlang("comp", "Text_esMx").getStringJSON("txtBackupRestoreWorking"));
		compBacks.val_db.getStyleClass().clear();
		compBacks.val_db.getStyleClass().addAll("validator", "validator--main", "validator--success");
		compBacks.val_db.setMinHeight(Control.USE_PREF_SIZE);
		compBacks.val_db.setVisible(true);
		
		Tool_ReqJSON lang = Mix_Lang.getlang("comp", "Notification_esMx");
		
		View_Backups viewBack = View_Backups.init();
		DirectoryChooser dc = new DirectoryChooser();
		File selectedDirectory = dc.showDialog(viewBack.getWindow());
		
		if (selectedDirectory != null) {
      Process p = null;
		  
      try {
		    
			  if (Inter_DB.password.equals(""))
          p = Runtime.getRuntime().exec("mysqldump -u " + Inter_DB.user + " " + Inter_DB.database);
			  else
          p = Runtime.getRuntime().exec("mysqldump -u " + Inter_DB.user + " -p" + Inter_DB.password + " " + Inter_DB.database);
        
				// Para mostrar los errores de SQL
				Tool_ErrorsBackRestore.lauchErrorsDB(p.getErrorStream());

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy_HH-mm-ss");
        LocalDateTime dateTime = LocalDateTime.of(LocalDate.now(), LocalTime.now());
        PrintWriter writer = new PrintWriter(selectedDirectory.getAbsolutePath() + "/baseDatos_"+ dateTime.format(formatter) +".sql", "UTF-8");
        
        BufferedReader reader = new BufferedReader(new InputStreamReader(p.getInputStream()));
        String text = reader.readLine();
        
        while (text != null) {
          writer.println(text);
          text = reader.readLine();
        }
        p.waitFor();
				
        reader.close();
        writer.flush();
        writer.close();

        Mix_Components.notificationComponent(
          "normal",
          lang.getStringJSON("backupDBTitle"),
          Tool_Text.findAndReplaceAll("##", dateTime.toString(), lang.getStringJSON("backupDBContent"))
            + Tool_Text.findAndReplaceAll("##", selectedDirectory.getAbsolutePath(), lang.getStringJSON("backupDBContentDirectory")),
          12
        );

        Mix_Menu.clearValidator();
				
			} catch (IOException | InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} finally {
			  p.destroy();
      }
		} else Mix_Menu.clearValidator();
		
		Mix_Menu.enableDisableElements(false);

	}
	
	public static void restore () {
		Mix_Menu.clearValidator();
		Mix_Menu.enableDisableElements(true);
		
		Comp_Backups compBacks = Comp_Backups.init();
		compBacks.val_db.setText(Mix_Lang.getlang("comp", "Text_esMx").getStringJSON("txtBackupRestoreWorking"));
		compBacks.val_db.getStyleClass().clear();
		compBacks.val_db.getStyleClass().addAll("validator", "validator--main", "validator--success");
		compBacks.val_db.setMinHeight(Control.USE_PREF_SIZE);
		compBacks.val_db.setVisible(true);
		
		Tool_ReqJSON lang = Mix_Lang.getlang("comp", "Notification_esMx");
		
		View_Backups viewBack = View_Backups.init();
		FileChooser fc = new FileChooser();
		File selectedFile = fc.showOpenDialog(viewBack.getWindow());
		
		if (selectedFile != null) {
      Process p = null;
      
			try {
			  if (Inter_DB.password.equals(""))
          p = Runtime.getRuntime().exec("mysql -u " + Inter_DB.user + " " + Inter_DB.database); 
			  else
          p = Runtime.getRuntime().exec("mysql -u " + Inter_DB.user + " -p" + Inter_DB.password + " " + Inter_DB.database);
				
				// Para mostrar los errores de SQL
				Tool_ErrorsBackRestore.lauchErrorsDB(p.getErrorStream());
						
				OutputStream os = p.getOutputStream();
				FileInputStream fis = new FileInputStream(selectedFile.getAbsolutePath());

				byte[] buffer = new byte[1024]; // Lo que hace es leer el archivo por grupos de bytes en lugar de 1 en uno (es como si leyera por un grupo de letras en lugar de 1 por uno)
				
				int i = fis.read(buffer);

				while (i > 0) {
					os.write(buffer, 0, i);
					i = fis.read(buffer);
				}
				
				os.flush(); // para vaciar el buffer de salida
				os.close();
				fis.close();

				if (p.waitFor() == 0) {
					Mix_Components.notificationComponent(
							"normal", 
							lang.getStringJSON("restoreDBTitle"),
							lang.getStringJSON("restoreDBContent"),
							12
							);
					
					View_Home viewHome = View_Home.init(null);
					Mix_Board.refreshResume(viewHome.getWindow().getWidth(), viewHome.getWindow().getHeight(), null);
					
					Tool_ConnectionDB conn = Tool_ConnectionDB.init();
					try {
						Mix_FormAgenda.chargeCategories(conn.getConnection());
						Mix_FormCategory.chargeCategories(conn.getConnection());
						Mix_Board.enableDisableControls();
					} catch (Exce_DAO_Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} finally {
						conn.closeConnection();
						Mix_Menu.clearValidator();
					}
				}
				
			} catch (IOException | InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} finally {
			  p.destroy();
      }
		} else Mix_Menu.clearValidator();
		
		Mix_Menu.enableDisableElements(false);
	}
	
}
