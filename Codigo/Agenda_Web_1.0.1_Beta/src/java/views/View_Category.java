package views;

import inter.Inter_Views;
import javafx.scene.Scene;
import javafx.stage.Stage;
import mixins.Mix_Lang;
import tools.Tool_Image;
import tools.Tool_ReqJSON;
import views.category.listeners.List_FormCategory;
import views.category.mixins.Mix_FormCategory;
import views.category.scenes.Sce_FormCategory;

/**
 *
 * Archivo View_Category.java
 * @author Joaquin Reyes Sanchez [Hakyn Seyer] <joaquin.seyer21@gmail.com>
 * Creado el día 28/01/19 a las 12:03 AM
 * @license This software is MIT licensed (see LICENSE)
 * 
*/

public class View_Category implements Inter_Views {

	private static View_Category viewCategory = null;
	
	private Stage window;
	
	private Tool_ReqJSON lang = null;
	
	private final double winWidth = 300;
	private final double winHeight = 275;
	
	public static View_Category init () {
		if (View_Category.viewCategory == null) 
			View_Category.viewCategory = new View_Category();
		
		return View_Category.viewCategory;
	}
	
	private View_Category () {
		this.window = new Stage();
		
		this.lang = Mix_Lang.getlang("sce", "Scene_esMx");
		
		if (this.lang != null) {
			this.window.setTitle(this.lang.getStringJSON("CategoryTitle"));
			
			this.chargeScene("");
			
			this.listenersWindow();
		} else System.out.println("The language did not load");
	}

	@Override
	public void chargeScene(String reqScene) {
		Scene scene = null;
		
		switch (reqScene) {
			default:
				Sce_FormCategory sceneForm = Sce_FormCategory.init(this.winWidth, this.winHeight);
				
				scene = sceneForm.buildingScene();
		}
		
		if (scene != null) {
			this.window.setScene(scene);
			this.window.setResizable(false);

      this.window.getIcons().add(Tool_Image.getImage("app", "aplicacion", "png"));
			
			List_FormCategory.init();
			
		} else System.out.println("You should add a scene :(");
	}

	@Override
	public void listenersWindow() {
		this.window.setOnCloseRequest(e -> Mix_FormCategory.closeWindow());
	}

	@Override
	public Stage getWindow() {
		return this.window;
	}
	
}
