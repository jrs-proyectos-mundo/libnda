package views;

import javafx.stage.Stage;
import inter.Inter_Views;
import javafx.scene.Scene;
import tools.Tool_Image;
import tools.Tool_ReqJSON;

import mixins.Mix_Lang;

import views.home.scenes.*;

import views.home.listeners.*;

/**
 *
 * Archivo View_Home.java
 * @author Joaquin Reyes Sanchez [Hakyn Seyer] <joaquin.seyer21@gmail.com>
 * Creado el día 28/01/19 a las 12:04 AM
 * @license This software is MIT licensed (see LICENSE)
 * 
*/

public class View_Home implements Inter_Views {

  private static View_Home viewHome = null;

  private Stage window;

  private Tool_ReqJSON lang = null;

  private final double winWidth = 400;
  private final double winHeight = 510;

  /**
   * 
   * @param mainStage El stage principal de la aplicación
   * @return La instancía única bajo el paradigma Singleton
   */
  public static View_Home init (Stage mainStage) {

    if (View_Home.viewHome == null)
      View_Home.viewHome =  new View_Home(mainStage);

    return View_Home.viewHome;

  }
  
  private View_Home (Stage mainStage) {
    this.window = mainStage;
    
    this.lang = Mix_Lang.getlang("sce", "Scene_esMx");
    
    if (this.lang != null) {
      this.window.setTitle(this.lang.getStringJSON("HomeTitle"));

      this.chargeScene("");
      
      this.listenersWindow();

    } else System.out.println("The language did not load");
  }

  @Override
  public void chargeScene (String reqScene) {
    Scene scene = null;

    switch (reqScene) {
      case "information":
        Sce_Information sceneInformation = Sce_Information.init(this.winWidth, this.winHeight);
        
        scene = sceneInformation.buildingScene();
        break;
        
      case "table":
      	Sce_Table sceneTable = Sce_Table.init(this.winWidth, this.winHeight);
      	
      	scene = sceneTable.buildingScene();
      	
      	List_Table.init();
      	break;
      
      default:
        Sce_Board sceneBoard = Sce_Board.init(this.winWidth, this.winHeight);

        scene = sceneBoard.buildingScene();
        
        List_Board.init();
    }
    
    if (scene != null) {
      this.window.setScene(scene);
      this.window.setResizable(false);
      
      this.window.getIcons().add(Tool_Image.getImage("app", "aplicacion", "png"));
      
    } else System.out.println("You should add a scene :(");
  }

  @Override
  public Stage getWindow () {
    return this.window;
  }

  @Override
  public void listenersWindow() {
  	this.window.setOnCloseRequest(e -> System.exit(0));
  }
}
