package views;

import inter.Inter_Views;
import javafx.scene.Scene;
import javafx.stage.Stage;
import mixins.Mix_Lang;
import tools.Tool_Image;
import tools.Tool_ReqJSON;
import views.backups.listeners.List_Menu;
import views.backups.mixins.Mix_Menu;
import views.backups.scenes.Sce_Menu;

/**
 *
 * Archivo View_Backups.java
 * @author Joaquin Reyes Sanchez [Hakyn Seyer] <joaquin.seyer21@gmail.com>
 * Creado el día 28/01/19 a las 12:03 AM
 * @license This software is MIT licensed (see LICENSE)
 * 
*/

public class View_Backups implements Inter_Views {
	
	private static View_Backups viewBackups = null;
	
	private Stage window;
	
	private Tool_ReqJSON lang = null;
	
	private final double winWidth = 300;
	private final double winHeight = 350;
	
	
	public static View_Backups init () {
		if (View_Backups.viewBackups == null)
			View_Backups.viewBackups = new View_Backups();
		
		return View_Backups.viewBackups;
	}
	
	private View_Backups () {
		this.window = new Stage();
		
		this.lang = Mix_Lang.getlang("sce", "Scene_esMx");
		
		if (this.lang != null) {
			this.window.setTitle(this.lang.getStringJSON("BackupsTitle"));
			
			this.chargeScene("");
			
			this.listenersWindow();
		} else System.out.println("The language did not load");
	}

	@Override
	public void chargeScene(String reqScene) {
		Scene scene = null;
		
		switch (reqScene) {
			default:
				Sce_Menu sceneMenu = Sce_Menu.init(this.winWidth, this.winHeight);
				
				scene = sceneMenu.buildingScene();
		}
		
		if (scene != null) {
			this.window.setScene(scene);
			this.window.setResizable(false);

      this.window.getIcons().add(Tool_Image.getImage("app", "aplicacion", "png"));
			
			List_Menu.init();
		} else System.out.println("You should add a scene :(");
		
	}

	@Override
	public void listenersWindow() {
		this.window.setOnCloseRequest(e -> Mix_Menu.closeWindow());
	}

	@Override
	public Stage getWindow() {
		return this.window;
	}
	
}
