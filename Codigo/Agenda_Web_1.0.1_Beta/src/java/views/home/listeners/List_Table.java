package views.home.listeners;

import views.home.Comp_Table;
import views.registry.Comp_FormAgenda;
import views.registry.actions.Act_CategoryAgenda;
import javafx.scene.control.TableRow;
import data.Data_Agenda;
import exceptions.Exce_DAO_Exception;
import dao.DAO_Management;

import tools.Tool_ConnectionDB;

import views.registry.mixins.Mix_FormAgenda;
import views.home.mixins.Mix_Table;

/**
 *
 * Archivo List_Table.java
 * @author Joaquin Reyes Sanchez [Hakyn Seyer] <joaquin.seyer21@gmail.com>
 * Creado el día 28/01/19 a las 12:00 AM
 * @license This software is MIT licensed (see LICENSE)
 * 
*/

public class List_Table {

	private static List_Table listTable = null;
		
	private Comp_Table compTable = Comp_Table.init();
	private Comp_FormAgenda compFormAgenda = Comp_FormAgenda.init();
	
	private Data_Agenda agendaSelected = null;
	
	public static List_Table init () {
		if (List_Table.listTable == null)
			List_Table.listTable = new List_Table();
		
		return List_Table.listTable;
	}
			
	
	private List_Table () {
		new List_TableMain();
		new List_Controls();
	}
	
	/**
	 * Envia los datos de la fila seleccionada al formulario de modificación
	 */
	private void sendDataAgendaToForm () {
		Data_Agenda data = List_Table.this.agendaSelected;
		
		List_Table.this.compFormAgenda.frm_web_text.setText(data.getWeb());
		if (data.getAlias() != null)
			List_Table.this.compFormAgenda.frm_alias_text.setText(data.getAlias());
		List_Table.this.compFormAgenda.frm_email_text.setText(data.getEmail());
		List_Table.this.compFormAgenda.frm_password_text.setText(data.getPassword());
		Mix_FormAgenda.changeStatusCategory(true);
		List_Table.this.compFormAgenda.frm_category_select.setValue(data.getDataCategory());;
		if (data.getDetails() != null)
			List_Table.this.compFormAgenda.frm_details_text.setText(data.getDetails());
		
		List_Table.this.compFormAgenda.setOldDataAgenda(data);
		
		Mix_FormAgenda.statusForm(1);
		Mix_FormAgenda.clearValidators(true);
		Mix_FormAgenda.clearValidators(false);
	}
	
	private class List_TableMain {
		
		private List_TableMain () {
			this.selectRow();
//			this.editAgenda();
		}
		
		@SuppressWarnings("unused")
		private void editAgenda () {
			// Listener que escucha cuando el usuario hace doble click en una fila de la tabla
			// Hay un Error cuando combinas esta funcionalidad con un expander de TableRowExpanderColumn de la libreria controlsfx (no logra desplegar nada del expander, por lo que hay que decidir por mantener una funcionalidad, este listener o el expander)
			List_Table
			.this
			.compTable
			.tab_main
			.setRowFactory(e -> {
				TableRow<Data_Agenda> row = new TableRow<>();
				row.setOnMouseClicked(event -> {
					if (event.getClickCount() == 2 && (!row.isEmpty())) {
						List_Table.this.agendaSelected = row.getItem();
						
						List_Table.this.sendDataAgendaToForm();
					}
				});
				
				return row;
			});
		}
		
		private void selectRow () {
			List_Table
				.this
				.compTable
				.tab_main
				.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
					if (newSelection != null) {
						List_Table.this.agendaSelected = newSelection;
						
						List_Table.this.compTable.cont_btns_edit.setDisable(false);
						List_Table.this.compTable.cont_btns_delete.setDisable(false);
					}
				});
		}
		
	}
	
	private class List_Controls {
		
		private List_Controls () {
			this.searchAgenda();
			this.newAgenda();
			this.editAgenda();
			this.deleteAgenda();
		}
		
		private void searchAgenda () {
			List_Table
				.this
				.compTable
				.sea_area_text
				.setOnKeyPressed(e -> {
					String data = List_Table.this.compTable.sea_area_text.getText();
					
					if (!data.isEmpty()) {
						Tool_ConnectionDB conn = Tool_ConnectionDB.init();					
						try {
							DAO_Management manaDB = DAO_Management.init(conn.getConnection());
							
							List_Table.this.compTable.getObsDataAgenda().setAll(manaDB.daoAgenda().searchAgenda(data + e.getText()));
						} catch (Exce_DAO_Exception e2) {
							e2.printStackTrace();
						} finally {
							conn.closeConnection();
						}						
					} else Mix_Table.currentTable();
					
				});
		}
		
		private void newAgenda () {
			List_Table
				.this
				.compTable
				.cont_btns_new
				.setOnAction(e -> {					
					Mix_FormAgenda.statusForm(0);

					List_Table.this.compFormAgenda.setOldDataAgenda(null);
					
					Mix_FormAgenda.cleanForm();
				});
		}
		
		private void editAgenda () {	
			List_Table
				.this
				.compTable
				.cont_btns_edit
				.setOnAction(e -> {
					if (List_Table.this.agendaSelected != null)
						List_Table.this.sendDataAgendaToForm();
				});
		}
		
		private void deleteAgenda () {
			List_Table
				.this
				.compTable
				.cont_btns_delete
				.setOnAction(e -> {
					if (List_Table.this.agendaSelected != null) {
						Mix_FormAgenda.statusForm(2);
						
						new Act_CategoryAgenda(List_Table.this.agendaSelected);
					}
				});
		}
		
	}
	
}
