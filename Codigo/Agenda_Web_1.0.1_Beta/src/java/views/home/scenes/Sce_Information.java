package views.home.scenes;

import javafx.scene.Scene;
import javafx.geometry.Pos;
import javafx.beans.binding.Bindings;

import tools.Tool_ReqJSON;
import views.home.Comp_Information;
import views.registry.Comp_FormAgenda;

import mixins.Mix_Lang;

import data.Data_Category;
import inter.Inter_Scenes;

/**
 *
 * Archivo Sce_Information.java
 * @author Joaquin Reyes Sanchez [Hakyn Seyer] <joaquin.seyer21@gmail.com>
 * Creado el día 28/01/19 a las 12:01 AM
 * @license This software is MIT licensed (see LICENSE)
 * 
*/

public class Sce_Information implements Inter_Scenes {
	
	private static Sce_Information sceneInformation = null;
	
	private double homeWidth = 0, homeHeight = 0;
	private int fieldsHeight = 25, HBoxSpacing = 5;
	
	private String classHBox = "information__group", classHBoxLabel = "information__group__label", classHBoxLabelTitle = "information__group__label--title", classHBoxLabelText = "information__group__label--text";
	
	private Tool_ReqJSON dataAgendaLang = null;
	
	private Scene homeScene = null;
	
	private Comp_Information compInformation = Comp_Information.init();
	private Comp_FormAgenda compFormAgenda = Comp_FormAgenda.init();
	
	/**
	 * 
	 * @param homeWidth Anchura de la ventana home
	 * @param homeHeight Altura de la ventana home
	 * @return Instancía Sce_Information
	 */
	public static Sce_Information init(double homeWidth, double homeHeight) {
		if (Sce_Information.sceneInformation == null)
			Sce_Information.sceneInformation = new Sce_Information(homeWidth, homeHeight);
		
		return Sce_Information.sceneInformation;
	}
	
	private Sce_Information (double homeWidth, double homeHeight) {
		this.homeWidth = homeWidth;
		this.homeHeight = homeHeight;
		
		this.dataAgendaLang = Mix_Lang.getlang("data", "Agenda_esMx");
	}

	@Override
	public Scene buildingScene() {
		if (this.homeScene == null) {
			this.compInformation.main_scroll.setPrefSize(this.homeWidth, this.homeHeight);
			this.compInformation.main_scroll.setFitToWidth(true);
			
			this.compInformation.main_layout.setSpacing(20);
			this.compInformation.main_layout.setPrefSize(this.homeWidth, this.homeHeight);
			this.compInformation.main_layout.getStyleClass().add("information");
			this.compInformation.main_layout.setAlignment(Pos.CENTER);
			
			this.boxWeb();
			this.boxAlias();
			this.boxEmail();
			this.boxPassword();
			this.boxCategory();
			this.boxDetails();
			
			this.compInformation.main_layout.getChildren().addAll(
					this.compInformation.box_web,
					this.compInformation.box_alias,
					this.compInformation.box_email,
					this.compInformation.box_password,
					this.compInformation.box_category,
					this.compInformation.box_details
			);
			
			this.compInformation.main_scroll.setContent(this.compInformation.main_layout);
			
			this.homeScene = new Scene(this.compInformation.main_scroll, this.homeWidth, this.homeHeight);
			this.homeScene.getStylesheets().add(this.getClass().getResource("/styles/sce/Home_Information.css").toString());
		}
		
		return this.homeScene;
	}
	
	private void boxWeb () {
		this.compInformation.box_web.setSpacing(this.HBoxSpacing);
		this.compInformation.box_web.getStyleClass().add(this.classHBox);
		
		this.compInformation.box_web_title.getStyleClass().addAll(this.classHBoxLabel, this.classHBoxLabelTitle);
		this.compInformation.box_web_title.setText(this.dataAgendaLang.getStringJSON("webTitle"));
		this.compInformation.box_web_title.setPrefSize(this.homeWidth * .30, this.fieldsHeight);
		
		this.compInformation.box_web_text.getStyleClass().addAll(this.classHBoxLabel, this.classHBoxLabelText);
		this.compInformation.box_web_text.setPrefWidth(this.homeWidth * .60);
		this.compInformation.box_web_text.setMinHeight(this.fieldsHeight);
		this.compInformation.box_web_text.setWrapText(true);
		this.compInformation.box_web_text.textProperty().bind(this.compFormAgenda.frm_web_text.textProperty());
		
		this.compInformation.box_web.getChildren().addAll(
				this.compInformation.box_web_title,
				this.compInformation.box_web_text
				);
		
	}
	
	private void boxAlias () {
		this.compInformation.box_alias.setSpacing(this.HBoxSpacing);
		this.compInformation.box_alias.getStyleClass().add(this.classHBox);
		
		this.compInformation.box_alias_title.getStyleClass().addAll(this.classHBoxLabel, this.classHBoxLabelTitle);
		this.compInformation.box_alias_title.setText(this.dataAgendaLang.getStringJSON("aliasTitle"));
		this.compInformation.box_alias_title.setPrefSize(this.homeWidth * .30, this.fieldsHeight);
		
		this.compInformation.box_alias_text.getStyleClass().addAll(this.classHBoxLabel, this.classHBoxLabelText);
		this.compInformation.box_alias_text.setPrefWidth(this.homeWidth * .60);
		this.compInformation.box_alias_text.setMinHeight(this.fieldsHeight);
		this.compInformation.box_alias_text.setWrapText(true);
		this.compInformation.box_alias_text.textProperty().bind(this.compFormAgenda.frm_alias_text.textProperty());
		
		this.compInformation.box_alias.getChildren().addAll(
				this.compInformation.box_alias_title,
				this.compInformation.box_alias_text
				);
		
	}
	
	private void boxEmail () {
		this.compInformation.box_email.setSpacing(this.HBoxSpacing);
		this.compInformation.box_email.getStyleClass().add(this.classHBox);
		
		this.compInformation.box_email_title.getStyleClass().addAll(this.classHBoxLabel, this.classHBoxLabelTitle);
		this.compInformation.box_email_title.setText(this.dataAgendaLang.getStringJSON("emailTitle"));
		this.compInformation.box_email_title.setPrefSize(this.homeWidth * .30, this.fieldsHeight);
		
		this.compInformation.box_email_text.getStyleClass().addAll(this.classHBoxLabel, this.classHBoxLabelText);
		this.compInformation.box_email_text.setPrefWidth(this.homeWidth * .60);
		this.compInformation.box_email_text.setMinHeight(this.fieldsHeight);
		this.compInformation.box_email_text.setWrapText(true);
		this.compInformation.box_email_text.textProperty().bind(this.compFormAgenda.frm_email_text.textProperty());
		
		this.compInformation.box_email.getChildren().addAll(
				this.compInformation.box_email_title,
				this.compInformation.box_email_text
				);
		
	}
	
	private void boxPassword () {
		this.compInformation.box_password.setSpacing(this.HBoxSpacing);
		this.compInformation.box_password.getStyleClass().add(this.classHBox);
		
		this.compInformation.box_password_title.getStyleClass().addAll(this.classHBoxLabel, this.classHBoxLabelTitle);
		this.compInformation.box_password_title.setText(this.dataAgendaLang.getStringJSON("passTitle"));
		this.compInformation.box_password_title.setPrefSize(this.homeWidth * .30, this.fieldsHeight);
		
		this.compInformation.box_password_text.getStyleClass().addAll(this.classHBoxLabel, this.classHBoxLabelText);
		this.compInformation.box_password_text.setPrefWidth(this.homeWidth * .60);
		this.compInformation.box_password_text.setMinHeight(this.fieldsHeight);
		this.compInformation.box_password_text.setWrapText(true);
		this.compInformation.box_password_text.textProperty().bind(this.compFormAgenda.frm_password_text.textProperty());
		
		this.compInformation.box_password.getChildren().addAll(
				this.compInformation.box_password_title,
				this.compInformation.box_password_text
				);
		
	}
	
	private void boxCategory () {
		this.compInformation.box_category.setSpacing(this.HBoxSpacing);
		this.compInformation.box_category.getStyleClass().add(this.classHBox);
		
		this.compInformation.box_category_title.getStyleClass().addAll(this.classHBoxLabel, this.classHBoxLabelTitle);
		this.compInformation.box_category_title.setText(this.dataAgendaLang.getStringJSON("categoryTitle"));
		this.compInformation.box_category_title.setPrefSize(this.homeWidth * .30, this.fieldsHeight);
		
		this.compInformation.box_category_text.getStyleClass().addAll(this.classHBoxLabel, this.classHBoxLabelText);
		this.compInformation.box_category_text.setPrefWidth(this.homeWidth * .60);
		this.compInformation.box_category_text.setMinHeight(this.fieldsHeight);
		this.compInformation.box_category_text.setWrapText(true);
		this.compInformation.box_category_text.textProperty().bind(
				Bindings.createStringBinding(() -> {
					if (this.compFormAgenda.frm_category_select.getValue() != null)
						return ((Data_Category)this.compFormAgenda.frm_category_select.getValue()).getName();
					else
						return this.compFormAgenda.frm_category_text.getText();
				}, this.compFormAgenda.frm_category_select.valueProperty(), this.compFormAgenda.frm_category_text.textProperty())
		);
		
		this.compInformation.box_category.getChildren().addAll(
				this.compInformation.box_category_title,
				this.compInformation.box_category_text
				);
		
	}
	
	private void boxDetails () {
		this.compInformation.box_details.setSpacing(this.HBoxSpacing);
		this.compInformation.box_details.getStyleClass().add(this.classHBox);
		
		this.compInformation.box_details_title.getStyleClass().addAll(this.classHBoxLabel, this.classHBoxLabelTitle);
		this.compInformation.box_details_title.setText(this.dataAgendaLang.getStringJSON("detailsTitle"));
		this.compInformation.box_details_title.setPrefSize(this.homeWidth * .30, this.fieldsHeight);
		
		this.compInformation.box_details_text.getStyleClass().addAll(this.classHBoxLabel, this.classHBoxLabelText);
		this.compInformation.box_details_text.setPrefWidth(this.homeWidth * .60);
		this.compInformation.box_details_text.setMinHeight(this.fieldsHeight);
		this.compInformation.box_details_text.setWrapText(true);
		this.compInformation.box_details_text.textProperty().bind(this.compFormAgenda.frm_details_text.textProperty());
		
		this.compInformation.box_details.getChildren().addAll(
				this.compInformation.box_details_title,
				this.compInformation.box_details_text
		);
		
	}
	
}