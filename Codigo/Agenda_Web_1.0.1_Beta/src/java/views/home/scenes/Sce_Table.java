package views.home.scenes;

import org.controlsfx.control.table.TableRowExpanderColumn;

import com.sun.javafx.scene.control.skin.TableHeaderRow;

import data.Data_Agenda;
import inter.Inter_Scenes;
import javafx.beans.property.ReadOnlyObjectWrapper;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.TableCell;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import mixins.Mix_Lang;
import tools.Tool_Image;
import tools.Tool_ReqJSON;
import views.home.Comp_Table;

/**
 *
 * Archivo Sce_Table.java
 * @author Joaquin Reyes Sanchez [Hakyn Seyer] <joaquin.seyer21@gmail.com>
 * Creado el día 28/01/19 a las 12:01 AM
 * @license This software is MIT licensed (see LICENSE)
 * 
*/

@SuppressWarnings("restriction")
public class Sce_Table implements Inter_Scenes {

	private static Sce_Table sceneTable = null;
	
	private double homeWidth = 0, homeHeight = 0;
	private final double colWidth = 150, btnHeight = 50;
	
	private final String classTableColumn = "table__main__column", classTableColumnCell ="table__main__column__cell", classControlsButtons = "controls__button";
	
	@SuppressWarnings("unused")
	private Tool_ReqJSON dataAgendaLang = null, buttonsLang = null, textlang = null;
	
	private Scene tableScene = null;
	
	private Comp_Table compTable = Comp_Table.init();
	
	/**
	 * 
	 * @param homeWidth Anchura de la ventana home
	 * @param homeHeight Altura de la ventana home
	 * @return Instancía Sce_Table
	 */
	public static Sce_Table init (double homeWidth, double homeHeight) {
		if (Sce_Table.sceneTable == null)
			Sce_Table.sceneTable = new Sce_Table(homeWidth, homeHeight);
		
		return Sce_Table.sceneTable;
	}
	
	private Sce_Table (double homeWidth, double homeHeight) {
		this.homeWidth = homeWidth;
		this.homeHeight = homeHeight;
		
		this.dataAgendaLang = Mix_Lang.getlang("data", "Agenda_esMx");
		this.buttonsLang = Mix_Lang.getlang("comp", "Buttons_esMx");
		this.textlang = Mix_Lang.getlang("comp", "Text_esMx");
	}
	
	@Override
	public Scene buildingScene() {
		if (this.tableScene == null) {
			this.compTable.main_layout.setPrefSize(this.homeWidth, this.homeHeight);
			this.compTable.main_layout.setSpacing(10);
			this.compTable.main_layout.getStyleClass().add("table");
			this.compTable.main_layout.setPadding(new Insets(10, 10, 10, 10));
			
			this.tableMain();
			this.searchArea();
			this.controlButtons();
			
			this.compTable.main_layout.getChildren().addAll(
					this.compTable.tab_main,
					this.compTable.sea_area,
					this.compTable.cont_btns
			);
			
			this.tableScene = new Scene (this.compTable.main_layout, this.homeWidth, this.homeHeight);
			this.tableScene.getStylesheets().add(this.getClass().getResource("/styles/sce/Home_Table.css").toString());
		}		
		
		return this.tableScene;
	}

	private void tableMain () {
		this.compTable.tab_main.setPrefSize(this.homeWidth, this.homeHeight * .75);
		this.compTable.tab_main.getStyleClass().add("table__main");
		
		// Codigo para desactivar el reordenamiento de las columnas
		this.compTable.tab_main.widthProperty().addListener(new ChangeListener<Number>() {

			@Override
			public void changed(ObservableValue<? extends Number> observable, Number oldValue, Number newValue) {
				
				TableHeaderRow header = (TableHeaderRow) Sce_Table.this.compTable.tab_main.lookup("TableHeaderRow");
				header.reorderingProperty().addListener(new ChangeListener<Boolean	>() {

					@Override
					public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {
						header.setReordering(false);
					}
				});
			}
			
		});
		
//	En esta linea se debería de llamar a la base de datos para recoger los datos de la tabla. Esta acción se movió al mixin "Mix_Table.currentTable" y es accionada en el listener "List_Board" en el evento de apertura de la venta formulario
		
		this.tableColumnExpander();
//		this.tableColumnId();
		this.tableColumnUser();
		this.tableColumnWeb();
		this.tableColumnEmail();
		this.tableColumnPassword();
//		this.tableColumnAlias();
//		this.tableColumnCategory();
//		this.tableColumnDetails();
	}
	
	@SuppressWarnings("unused")
	private void tableColumnId () {
		this.compTable.tab_main_id.setText(this.dataAgendaLang.getStringJSON("idTitle"));
		this.compTable.tab_main_id.setCellValueFactory(new PropertyValueFactory<>("id"));
		this.compTable.tab_main_id.setPrefWidth(this.colWidth / 2);
		this.compTable.tab_main_id.setResizable(false);
		this.compTable.tab_main_id.getStyleClass().add(this.classTableColumn);
		
		this.compTable.tab_main.getColumns().add(this.compTable.tab_main_id);
	}
	
	private void tableColumnUser () {
		this.compTable.tab_main_user.setText(this.dataAgendaLang.getStringJSON("userTitle"));
		this.compTable.tab_main_user.setMinWidth(this.colWidth);
		this.compTable.tab_main_user.getStyleClass().add(this.classTableColumn);
		
	// Codigo para hacer wrapping a la celda de acuerdo al contenido... Funciona cuando aún cuando de hace el rezisable
		this.compTable.tab_main_user.setCellValueFactory(param -> new ReadOnlyObjectWrapper<>(String.valueOf(param.getValue().getUser())));
		this.compTable.tab_main_user.setCellFactory(param -> {
			return new TableCell<Data_Agenda, String> () {
				@Override
				protected void updateItem (String item, boolean empty) {
					super.updateItem(item, empty);
					
					if (item == null) this.setGraphic(null);
					else {
						StackPane layout = new StackPane();
						layout.setAlignment(Pos.TOP_CENTER);
						
						Text text = new Text(item);
						text.getStyleClass().add(Sce_Table.this.classTableColumnCell);
						text.wrappingWidthProperty().bind(getTableColumn().widthProperty().subtract(35));
						this.setPrefHeight(text.getLayoutBounds().getHeight() + 10);

						layout.getChildren().add(text);
						
						this.setGraphic(layout);
					};
				}
			};
		});
		
//		this.compTable.tab_main.getColumns().add(this.compTable.tab_main_user);
	}

	private void tableColumnWeb () {
		this.compTable.tab_main_web.setText(this.dataAgendaLang.getStringJSON("webTitle"));
		this.compTable.tab_main_web.setMinWidth(this.colWidth);
		this.compTable.tab_main_web.getStyleClass().add(this.classTableColumn);
		
		// Codigo para hacer wrapping a la celda de acuerdo al contenido... Funciona cuando aún cuando de hace el rezisable
		this.compTable.tab_main_web.setCellValueFactory(param -> new ReadOnlyObjectWrapper<>(param.getValue().getWeb()));		
		this.compTable.tab_main_web.setCellFactory(param -> {
			return new TableCell<Data_Agenda, String> () {
				@Override
				protected void updateItem (String item, boolean empty) {
					super.updateItem(item, empty);
					
					if (item == null) {
						this.setGraphic(null);
					}	else {
						StackPane layout = new StackPane();
						layout.setAlignment(Pos.TOP_CENTER);
						
						Text text = new Text(item);
						text.getStyleClass().add(Sce_Table.this.classTableColumnCell);
						text.wrappingWidthProperty().bind(getTableColumn().widthProperty().subtract(35));
						this.setPrefHeight(text.getLayoutBounds().getHeight() + 10);

						layout.getChildren().add(text);
						
						this.setGraphic(layout);
					};
				}
			};
		});
		
		this.compTable.tab_main.getColumns().add(this.compTable.tab_main_web);
	}

	@SuppressWarnings("unused")
	private void tableColumnAlias () {
		this.compTable.tab_main_alias.setText(this.dataAgendaLang.getStringJSON("aliasTitle"));
		this.compTable.tab_main_alias.setMinWidth(this.colWidth);
		this.compTable.tab_main_alias.getStyleClass().add(this.classTableColumn);
		
	// Codigo para hacer wrapping a la celda de acuerdo al contenido... Funciona cuando aún cuando de hace el rezisable
		this.compTable.tab_main_alias.setCellValueFactory(param -> new ReadOnlyObjectWrapper<>(param.getValue().getAlias()));
		this.compTable.tab_main_alias.setCellFactory(param -> {
			return new TableCell<Data_Agenda, String> () {
				@Override
				protected void updateItem (String item, boolean empty) {
					super.updateItem(item, empty);
					
					if (item == null) {
						this.setGraphic(null);
					} else if (item.equals("")) {
						StackPane layout = new StackPane();
						layout.setAlignment(Pos.TOP_CENTER);
						
						Text text = new Text("---------");
						text.getStyleClass().add(Sce_Table.this.classTableColumnCell);
						text.wrappingWidthProperty().bind(getTableColumn().widthProperty().subtract(35));
						this.setPrefHeight(text.getLayoutBounds().getHeight() + 10);
						
						layout.getChildren().add(text);
						
						this.setGraphic(layout);
					} else {
						StackPane layout = new StackPane();
						layout.setAlignment(Pos.TOP_CENTER);
						
						Text text = new Text(item);
						text.getStyleClass().add(Sce_Table.this.classTableColumnCell);
						text.wrappingWidthProperty().bind(getTableColumn().widthProperty().subtract(35));
						this.setPrefHeight(text.getLayoutBounds().getHeight() + 10);

						layout.getChildren().add(text);
						
						this.setGraphic(layout);
					};
				}
			};
		});
		
		this.compTable.tab_main.getColumns().add(this.compTable.tab_main_alias);
	}

	private void tableColumnEmail () {
		this.compTable.tab_main_email.setText(this.dataAgendaLang.getStringJSON("emailTitle"));
		this.compTable.tab_main_email.setMinWidth(this.colWidth);
		this.compTable.tab_main_email.getStyleClass().add(this.classTableColumn);
		
	// Codigo para hacer wrapping a la celda de acuerdo al contenido... Funciona cuando aún cuando de hace el rezisable
		this.compTable.tab_main_email.setCellValueFactory(param -> new ReadOnlyObjectWrapper<>(param.getValue().getEmail()));
		this.compTable.tab_main_email.setCellFactory(param -> {
			return new TableCell<Data_Agenda, String> () {
				@Override
				protected void updateItem (String item, boolean empty) {
					super.updateItem(item, empty);
					
					if (item == null) this.setGraphic(null);
					else {
						StackPane layout = new StackPane();
						layout.setAlignment(Pos.TOP_CENTER);
						
						Text text = new Text(item);
						text.getStyleClass().add(Sce_Table.this.classTableColumnCell);
						text.wrappingWidthProperty().bind(getTableColumn().widthProperty().subtract(35));
						this.setPrefHeight(text.getLayoutBounds().getHeight() + 10);

						layout.getChildren().add(text);
						
						this.setGraphic(layout);
					};
				}
			};
		});
		
		this.compTable.tab_main.getColumns().add(this.compTable.tab_main_email);
	}

	private void tableColumnPassword () {
		this.compTable.tab_main_password.setText(this.dataAgendaLang.getStringJSON("passwordTitle"));
		this.compTable.tab_main_password.setMinWidth(this.colWidth);
		this.compTable.tab_main_password.getStyleClass().add(this.classTableColumn);
		
	// Codigo para hacer wrapping a la celda de acuerdo al contenido... Funciona cuando aún cuando de hace el rezisable
		this.compTable.tab_main_password.setCellValueFactory(param -> new ReadOnlyObjectWrapper<>(param.getValue().getPassword()));
		this.compTable.tab_main_password.setCellFactory(param -> {
			return new TableCell<Data_Agenda, String> () {
				@Override
				protected void updateItem (String item, boolean empty) {
					super.updateItem(item, empty);
					
					if (item == null) this.setGraphic(null);
					else {
						StackPane layout = new StackPane();
						layout.setAlignment(Pos.TOP_CENTER);
						
						Text text = new Text(item);
						text.getStyleClass().add(Sce_Table.this.classTableColumnCell);
						text.wrappingWidthProperty().bind(getTableColumn().widthProperty().subtract(35));
						this.setPrefHeight(text.getLayoutBounds().getHeight() + 10);

						layout.getChildren().add(text);
						
						this.setGraphic(layout);
					};
				}
			};
		});
		
		this.compTable.tab_main.getColumns().add(this.compTable.tab_main_password);
	}

	@SuppressWarnings("unused")
	private void tableColumnCategory () {
		this.compTable.tab_main_category.setText(this.dataAgendaLang.getStringJSON("categoryTitle"));
		this.compTable.tab_main_category.setMinWidth(this.colWidth);
		this.compTable.tab_main_category.getStyleClass().add(this.classTableColumn);
		
	// Codigo para hacer wrapping a la celda de acuerdo al contenido... Funciona cuando aún cuando de hace el rezisable
		this.compTable.tab_main_category.setCellValueFactory(param -> new ReadOnlyObjectWrapper<>(param.getValue().getDataCategory().getName()));
		this.compTable.tab_main_category.setCellFactory(param -> {
			return new TableCell<Data_Agenda, String> () {
				@Override
				protected void updateItem (String item, boolean empty) {
					super.updateItem(item, empty);
					
					if (item == null) this.setGraphic(null);
					else {
						StackPane layout = new StackPane();
						layout.setAlignment(Pos.TOP_CENTER);
						
						Text text = new Text(item);
						text.getStyleClass().add(Sce_Table.this.classTableColumnCell);
						text.wrappingWidthProperty().bind(getTableColumn().widthProperty().subtract(35));
						this.setPrefHeight(text.getLayoutBounds().getHeight() + 10);

						layout.getChildren().add(text);
						
						this.setGraphic(layout);
					};
				}
			};
		});
		
		this.compTable.tab_main.getColumns().add(this.compTable.tab_main_category);
	}

	@SuppressWarnings("unused")
	private void tableColumnDetails () {
		this.compTable.tab_main_details.setText(this.dataAgendaLang.getStringJSON("detailsTitle"));
		this.compTable.tab_main_details.setMinWidth(this.colWidth * 1.5);
		this.compTable.tab_main_details.getStyleClass().add(this.classTableColumn);
		
	// Codigo para hacer wrapping a la celda de acuerdo al contenido... Funciona cuando aún cuando de hace el rezisable
		this.compTable.tab_main_details.setCellValueFactory(param -> new ReadOnlyObjectWrapper<>(param.getValue().getDetails()));
		this.compTable.tab_main_details.setCellFactory(param -> {
			return new TableCell<Data_Agenda, String> () {
				@Override
				protected void updateItem (String item, boolean empty) {
					super.updateItem(item, empty);
					
					if (item == null) {
						this.setGraphic(null);
					} else if (item.equals("")) {
						StackPane layout = new StackPane();
						layout.setAlignment(Pos.TOP_CENTER);
						
						Text text = new Text("---------");
						text.getStyleClass().add(Sce_Table.this.classTableColumnCell);
						text.wrappingWidthProperty().bind(getTableColumn().widthProperty().subtract(35));
						this.setPrefHeight(text.getLayoutBounds().getHeight() + 10);
						
						layout.getChildren().add(text);
						
						this.setGraphic(layout);
					} else {
						StackPane layout = new StackPane();
						layout.setAlignment(Pos.TOP_CENTER);
						
						Text text = new Text(item);
						text.getStyleClass().add(Sce_Table.this.classTableColumnCell);
						text.wrappingWidthProperty().bind(getTableColumn().widthProperty().subtract(35));
						this.setPrefHeight(text.getLayoutBounds().getHeight() + 10);

						layout.getChildren().add(text);
						
						this.setGraphic(layout);
					};
				}
			};
		});
		
		this.compTable.tab_main.getColumns().add(this.compTable.tab_main_details);
	}
	
	private void tableColumnExpander () {
		this.compTable.tab_main_expander = new TableRowExpanderColumn<>(param -> {
			VBox moreData = new VBox(10);
			moreData.getStyleClass().add("table__row__expander");
			moreData.setPrefWidth(this.homeWidth);
			
			Data_Agenda agenda = param.getValue();
					
			if (agenda.getAlias() != null) {
				HBox gridAlias = new HBox(5);
				ImageView imgAlias = new ImageView(Tool_Image.getImage("app", "alias_blanco", "png"));
				Label contAlias = new Label(agenda.getAlias());
				contAlias.getStyleClass().add("table__row__expander__text");
				gridAlias.getChildren().addAll(imgAlias, contAlias);				
				moreData.getChildren().add(gridAlias);
			}

			HBox gridCategory = new HBox(5);
			ImageView imgCategory = new ImageView(Tool_Image.getImage("app", "categoria_blanco", "png"));
			Label contCategory = new Label(agenda.getDataCategory().getName());
			contCategory.getStyleClass().add("table__row__expander__text");
			gridCategory.getChildren().addAll(imgCategory, contCategory);
			moreData.getChildren().add(gridCategory);

			if (agenda.getDetails() != null) {
				HBox gridDetails = new HBox(5);
				ImageView imgDetails = new ImageView(Tool_Image.getImage("app", "notas_blanco", "png"));
				Label contDetails = new Label(agenda.getDetails());
				contDetails.getStyleClass().add("table__row__expander__text");
				contDetails.setWrapText(true);
				gridDetails.getChildren().addAll(imgDetails, contDetails);				

				moreData.getChildren().add(gridDetails);
			}
			
			
			return moreData;
		});
		
		this.compTable.tab_main.getColumns().add(this.compTable.tab_main_expander);
	}
	
	private void searchArea () {
		this.compTable.sea_area.setPrefSize(this.homeWidth, this.homeWidth * .10);
		this.compTable.sea_area.setSpacing(10);
		this.compTable.sea_area.setAlignment(Pos.CENTER);
		this.compTable.sea_area.getStyleClass().add("search");
		
		this.compTable.sea_area_text.setPrefSize(this.homeWidth, 15);
		this.compTable.sea_area_text.getStyleClass().add("search__text");
		this.compTable.sea_area_text.setPromptText(this.textlang.getStringJSON("txtSearch") + " " + this.dataAgendaLang.getStringJSON("webTitle") + ", " + this.dataAgendaLang.getStringJSON("aliasTitle") + ", " + this.dataAgendaLang.getStringJSON("emailTitle"));
		this.compTable.sea_area_text.setLeft(new ImageView(Tool_Image.getImage("app", "buscar_con_mano", "png")));
		
		this.compTable.sea_area.getChildren().add(this.compTable.sea_area_text);
	}
	
	private void controlButtons () {
		this.compTable.cont_btns.setPrefSize(this.homeWidth, this.homeHeight * .15);
		this.compTable.cont_btns.setSpacing(10);
		this.compTable.cont_btns.setAlignment(Pos.CENTER);
		this.compTable.cont_btns.getStyleClass().add("controls");
		
//		this.compTable.cont_btns_new.setText(this.buttonsLang.getStringJSON("btnNew"));
		this.compTable.cont_btns_new.setGraphic(new ImageView(Tool_Image.getImage("app", "nuevo", "png")));
		this.compTable.cont_btns_new.setPrefSize(this.homeWidth / 4, this.btnHeight);
		this.compTable.cont_btns_new.getStyleClass().add(this.classControlsButtons);
		
//		this.compTable.cont_btns_edit.setText(this.buttonsLang.getStringJSON("btnUpdate"));
		this.compTable.cont_btns_edit.setGraphic(new ImageView(Tool_Image.getImage("app", "editar", "png")));
		this.compTable.cont_btns_edit.setPrefSize(this.homeWidth / 4, this.btnHeight);
		this.compTable.cont_btns_edit.getStyleClass().addAll(this.classControlsButtons, "controls__button--edit");
		this.compTable.cont_btns_edit.setDisable(true);
		
//		this.compTable.cont_btns_delete.setText(this.buttonsLang.getStringJSON("btnDelete"));
		this.compTable.cont_btns_delete.setGraphic(new ImageView(Tool_Image.getImage("app", "basura", "png")));
		this.compTable.cont_btns_delete.setPrefSize(this.homeWidth / 4, this.btnHeight);
		this.compTable.cont_btns_delete.getStyleClass().addAll(this.classControlsButtons, "controls__button--delete");
		this.compTable.cont_btns_delete.setDisable(true);
		
		this.compTable.cont_btns.getChildren().addAll(
				this.compTable.cont_btns_new,
				this.compTable.cont_btns_edit,
				this.compTable.cont_btns_delete
		);
	}
	
}
