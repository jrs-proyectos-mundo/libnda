package views.home;

import data.Data_Agenda;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;
import org.controlsfx.control.textfield.CustomTextField;

/**
 *
 * Archivo Comp_Board.java
 * @author Joaquin Reyes Sanchez [Hakyn Seyer] <joaquin.seyer21@gmail.com>
 * Creado el día 28/01/19 a las 12:01 AM
 * @license This software is MIT licensed (see LICENSE)
 * 
*/

public class Comp_Board {
	// Variables
	private ObservableList<Data_Agenda> listAgendas = FXCollections.observableArrayList();

	
	// Main layout
	public final VBox main_layout = new VBox();
	
	// Área Resumen
		// Contenedores Resumen
		public final ScrollPane res_scroll = new ScrollPane();
		public final VBox res_container = new VBox();
		
	// Área Controles
		// Contenedor Controls
		public final GridPane cont_container = new GridPane();
		
		// Grupo Search
		public final CustomTextField cont_search_text = new CustomTextField();
		
		// Grupo botones
		public final Button cont_buttons_report = new Button();
		public final Button cont_buttons_dataBase = new Button();
		public final Button cont_buttons_openCategory = new Button();
		public final Button cont_buttons_openRegistry = new Button(); 
		public final Button cont_buttons_exit = new Button();
		
		// Copyright
    public final VBox info_container = new VBox();
    public final Label info_copyright = new Label();
    public final Label info_email = new Label();
		
	private static Comp_Board compHome = null;
	
	public static Comp_Board init () {
		if (Comp_Board.compHome == null)
			Comp_Board.compHome = new Comp_Board();
		
		return Comp_Board.compHome;
	}
	
	private Comp_Board () {}

	public ObservableList<Data_Agenda> getListAgendas() {
		return listAgendas;
	}
	
}
