package views.home.mixins;

import dao.DAO_Management;
import exceptions.Exce_DAO_Exception;
import tools.Tool_ConnectionDB;

import views.home.Comp_Table;

/**
 *
 * Archivo Mix_Table.java
 * @author Joaquin Reyes Sanchez [Hakyn Seyer] <joaquin.seyer21@gmail.com>
 * Creado el día 28/01/19 a las 12:00 AM
 * @license This software is MIT licensed (see LICENSE)
 * 
*/

public class Mix_Table {

	public static void cleanSearch () {
		Comp_Table compTable = Comp_Table.init();
		
		compTable.sea_area_text.clear();
	}
	
	public static void currentTable () {
		Comp_Table compTable = Comp_Table.init();
		
		Tool_ConnectionDB conn = Tool_ConnectionDB.init();		
		try {
			
			DAO_Management manaDB = DAO_Management.init(conn.getConnection());

			compTable.getObsDataAgenda().setAll(manaDB.daoAgenda().read());
		} catch (Exce_DAO_Exception e) {
			e.printStackTrace();
		} finally {
			conn.closeConnection();
		}
		
		compTable.sea_area_text.clear();
		
	}
	
}
