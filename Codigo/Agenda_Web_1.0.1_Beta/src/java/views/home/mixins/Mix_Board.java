package views.home.mixins;

import dao.DAO_Management;
import data.Data_Agenda;
import exceptions.Exce_DAO_Exception;
import javafx.collections.ObservableList;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Accordion;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TitledPane;
import javafx.scene.image.ImageView;
import javafx.scene.input.Clipboard;
import javafx.scene.input.ClipboardContent;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import mixins.Mix_Lang;
import tools.Tool_ConnectionDB;
import tools.Tool_Image;
import tools.Tool_ReqJSON;
import views.home.Comp_Board;

/**
 *
 * Archivo Mix_Board.java
 * @author Joaquin Reyes Sanchez [Hakyn Seyer] <joaquin.seyer21@gmail.com>
 * Creado el día 28/01/19 a las 12:00 AM
 * @license This software is MIT licensed (see LICENSE)
 * 
*/

public class Mix_Board {

	private static GridPane resumeCard (double homeWidth, double homeHeight, String[] titles, String[] moreTitles) {
		GridPane card = new GridPane();
		card.setPrefWidth(homeWidth);
		card.getStyleClass().add("board__resume__card");
		
		for (int i = 0; i < titles.length; i++) {
			String titleClipboard = titles[i];
			
			HBox boxTitles = new HBox(10);
			boxTitles.setPadding(new Insets(5, 5, 5, 5));
			boxTitles.setAlignment(Pos.CENTER);
			boxTitles.setPrefWidth(homeWidth);
			
			ImageView imageview = null;
			
			switch (i) {
				case 0:
					// Web
					boxTitles.getStyleClass().add("board__resume__card__first");
					imageview = new ImageView(Tool_Image.getImage("app", "web", "png"));
					break;
				case 1:
					// Email
					imageview = new ImageView(Tool_Image.getImage("app", "email", "png"));
					break;
				case 2:
					// Password
					imageview = new ImageView(Tool_Image.getImage("app", "contrasena", "png"));
					break;
				case 3:
					// alias
					imageview = new ImageView(Tool_Image.getImage("app", "alias", "png"));
					break;
			}
			
			if (titleClipboard != null) {
				// Para agregar todos los iconos excepto "web" a la izquierda del label
				if (i > 0) boxTitles.getChildren().add(imageview);
				
				Label labelTitles = new Label(titleClipboard);
				labelTitles.setPrefWidth(homeWidth * .80);
				if (i == 0) labelTitles.getStyleClass().addAll("board__resume__card__label", "board__resume__card__first__label");
				else labelTitles.getStyleClass().addAll("board__resume__card__label", "board__resume__card__label--title");
				boxTitles.getChildren().add(labelTitles);
				
				// Para agregar el icono web a la derecha del label
				if (i == 0) boxTitles.getChildren().add(imageview);
			}
			
			if (i > 0 && titleClipboard != null) {
				Button clipBoardTitles = new Button();
				clipBoardTitles.setGraphic(new ImageView(Tool_Image.getImage("app", "copiar", "png")));
				clipBoardTitles.getStyleClass().addAll("board__resume__card__button", "board__resume__card__button--clipboard");
				clipBoardTitles.setPrefWidth(homeWidth * .2);
				clipBoardTitles.setOnAction(e -> {
					Clipboard clipboard = Clipboard.getSystemClipboard();
					ClipboardContent content = new ClipboardContent();
					
					content.putString(titleClipboard);
					
					clipboard.setContent(content);
				});
			
				boxTitles.getChildren().add(clipBoardTitles);
			}
			
			card.add(boxTitles, 0, i);
			
		}
		
		VBox moreData = new VBox(10);
		moreData.getStyleClass().add("board__resume__more-data");
		moreData.setPrefWidth(homeWidth);
		
		for (int i = 0; i < moreTitles.length; i++) {
			if (moreTitles[i] != null) {
				
				HBox gridData = new HBox(10);
				gridData.setPadding(new Insets(5, 5, 5, 5));
				gridData.setAlignment(Pos.CENTER);
				
				ImageView imageviewplus = null;
				
				switch (i) {
				case 0:
					// Categoria
					imageviewplus = new ImageView(Tool_Image.getImage("app", "categoria", "png"));
					break;
				case 1:
					// Details
					imageviewplus = new ImageView(Tool_Image.getImage("app", "notas", "png"));
					break;
				}
				
				gridData.getChildren().add(imageviewplus);
				
				Label label = new Label(moreTitles[i]);
				label.setWrapText(true);
				label.setPrefWidth(homeWidth);
				label.getStyleClass().addAll("board__resume__card__label", "board__resume__card__label--title");
				
				gridData.getChildren().add(label);
				
				moreData.getChildren().add(gridData);
			}
		}
		
		Tool_ReqJSON textLang = Mix_Lang.getlang("comp", "Text_esMx");
		
		TitledPane panel = new TitledPane();
		panel.getStyleClass().add("board__resume__more-data__panel");
		panel.setPrefWidth(homeWidth);
		panel.setText(textLang.getStringJSON("txtMoreData"));
		panel.setContent(moreData);
		Accordion accordion = new Accordion();
		accordion.setPrefWidth(homeWidth);
		accordion.getPanes().add(panel);
		card.add(accordion, 0, titles.length + 1);
		
		return card;
	}
	
	/**
	 * 
	 * @param homeWidth Ancho de la ventana principal
	 * @param homeHeight Altura de la ventana principal
	 * @param dataList Lista Observable con los datos necesarios para rellenar el resume. IMPORTANTE, se puede colocar null y automáticamente buscará la última actualización global de los datos
	 */
	public static void refreshResume (double homeWidth, double homeHeight, ObservableList<Data_Agenda> dataList) {
		Comp_Board compBoard = Comp_Board.init();
		
		compBoard.res_container.getChildren().clear();

		if (dataList ==  null) {
			compBoard.getListAgendas().clear();
			Tool_ConnectionDB conn = Tool_ConnectionDB.init();
			
			try {
				DAO_Management manaDB = DAO_Management.init(conn.getConnection());
				
				compBoard.getListAgendas().addAll(manaDB.daoAgenda().read());
				
			} catch (Exce_DAO_Exception e) {
				e.printStackTrace();
			} finally {
				conn.closeConnection();
			}
			
			for (Data_Agenda agenda : compBoard.getListAgendas())			
				compBoard.res_container.getChildren().add(
						Mix_Board.resumeCard(
								homeWidth, 
								homeHeight,
								new String[] {
										agenda.getWeb(),
										agenda.getEmail(),
										agenda.getPassword(),
										agenda.getAlias()
								},
								new String [] {
										agenda.getDataCategory().getName(),
										agenda.getDetails()
								}
								)
						);
		} else {
			for (Data_Agenda agenda : dataList)
				compBoard.res_container.getChildren().add(
						Mix_Board.resumeCard(
								homeWidth, 
								homeHeight,
								new String[] {
										agenda.getWeb(),
										agenda.getEmail(),
										agenda.getPassword(),
										agenda.getAlias()
								},
								new String [] {
										agenda.getDataCategory().getName(),
										agenda.getDetails()
								}
								)
						);
		}
		
		
		
	}
	
	public static void enableDisableControls (boolean status) {
		Comp_Board compBoard = Comp_Board.init();
		
		if (compBoard.cont_buttons_openRegistry.isDisable() != status)
			compBoard.cont_buttons_openRegistry.setDisable(status);
		if (compBoard.cont_buttons_dataBase.isDisable() != status)
			compBoard.cont_buttons_dataBase.setDisable(status);
		if (!status) {
			if (compBoard.getListAgendas().size() > 0) {
				compBoard.cont_buttons_report.setDisable(status);
				compBoard.cont_buttons_openCategory.setDisable(status);
			}	else {
				compBoard.cont_buttons_report.setDisable(true);
				compBoard.cont_buttons_openCategory.setDisable(true);
			}
		} else {
			compBoard.cont_buttons_report.setDisable(status);
			compBoard.cont_buttons_openCategory.setDisable(status);
		}
		
	}

	public static void enableDisableControls () {
		Comp_Board compBoard = Comp_Board.init();
		
		if (compBoard.getListAgendas().size() > 0) {
			compBoard.cont_buttons_report.setDisable(false);
			compBoard.cont_buttons_openCategory.setDisable(false);
		} else {
			compBoard.cont_buttons_report.setDisable(true);
			compBoard.cont_buttons_openCategory.setDisable(true);
		}
		
	}
	
}
