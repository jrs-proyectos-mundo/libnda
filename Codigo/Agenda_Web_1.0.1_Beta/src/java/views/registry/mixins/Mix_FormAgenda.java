package views.registry.mixins;

import java.io.IOException;
import java.sql.Connection;
import java.util.Optional;

import org.json.simple.parser.ParseException;

import dao.DAO_Management;
import data.Data_Agenda;
import data.Data_Category;
import exceptions.Exce_DAO_Exception;
import javafx.collections.ObservableList;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import mixins.Mix_Components;
import tools.Tool_ReqJSON;
import views.View_Home;
import views.View_Registry;
import views.home.Comp_Table;
import views.registry.Comp_FormAgenda;

/**
 *
 * Archivo Mix_FormAgenda.java
 * @author Joaquin Reyes Sanchez [Hakyn Seyer] <joaquin.seyer21@gmail.com>
 * Creado el día 28/01/19 a las 12:02 AM
 * @license This software is MIT licensed (see LICENSE)
 * 
*/

public class Mix_FormAgenda {

  /**
   * 
   * @param isButton Indica si el accionador del evento fue un botón Button o nó
   */
  public static void returnWinDefaultHome (boolean isButton) {
    View_Registry winReg = View_Registry.init();
    
    Tool_ReqJSON lang = null;
    
    try {
      lang = new Tool_ReqJSON("comp" , "Alert_esMx");
    } catch (IOException e) {
      e.printStackTrace();
    } catch (ParseException e) {
      e.printStackTrace();
    }

    View_Home home = View_Home.init(null);
    
    if (isButton) {
      
      if (Mix_FormAgenda.completeDataForm(true) > 0) {
        Alert confirm = Mix_Components.confirmComponent(350, 75, lang.getStringJSON("alertExitFormTitle"), lang.getStringJSON("alertExitoFormContent"));
        
        confirm.initOwner(winReg.getWindow());
        
        Optional<ButtonType> result = confirm.showAndWait();
    
        if (result.get() == ButtonType.YES) {
          winReg.getWindow().close();
          
          home.getWindow().setWidth(400);
          home.chargeScene("");
        }
      } else {
        winReg.getWindow().close();
          
        home.getWindow().setWidth(400);
        home.chargeScene("");
      }

    } else {
    	home.getWindow().setWidth(400);
      home.chargeScene("");
    }

  }
  
  public static void cleanForm () {
  	Comp_FormAgenda compReg = Comp_FormAgenda.init();
  	
  	compReg.frm_web_text.clear();
  	compReg.frm_alias_text.clear();
  	compReg.frm_email_text.clear();
  	compReg.frm_password_text.clear();
  	compReg.frm_category_text.clear();
  	compReg.frm_category_select.setValue(null);
  	compReg.frm_details_text.clear();
  	
  	compReg.frm_web_text.requestFocus();
  }

  /**
   * 
   * @param allFields True para analizar todos los campos del formulario; False para descartar los campos siguientes: Alias y Detalles
   * @return
   */
  public static int completeDataForm (boolean allFields) {
    Comp_FormAgenda compReg = Comp_FormAgenda.init();
  	
  	int completeElements = 0;
    
    if (!compReg.frm_web_text.getText().equals(""))
    	completeElements++;
    
    if (!compReg.frm_email_text.getText().equals(""))
    	completeElements++;
    
    if (!compReg.frm_password_text.getText().equals(""))
    	completeElements++;
    
    if (!compReg.frm_category_text.getText().equals("") || compReg.frm_category_select.getValue() != null)
    	completeElements++;
    
    if (allFields) {
    	if (!compReg.frm_alias_text.getText().equals(""))
    		completeElements++;
    	
    	if (!compReg.frm_details_text.getText().equals(""))
    		completeElements++;
    }

    return completeElements;
  }

  /**
   * 
   * @param action Si es true se desactivan los elementos y si es false se activan
   */
  public static void disabledEnableElements (boolean action) {
  	Comp_FormAgenda compReg = Comp_FormAgenda.init();
  	
  	if (compReg.frm_web_text.isDisable() != action)
  		compReg.frm_web_text.setDisable(action);
  	
  	if (compReg.frm_alias_text.isDisable() != action)
  		compReg.frm_alias_text.setDisable(action);
  	
  	if (compReg.frm_email_text.isDisable() != action)
  		compReg.frm_email_text.setDisable(action);

  	if (compReg.frm_password_text.isDisable() != action)
  		compReg.frm_password_text.setDisable(action);
  	
  	if (compReg.frm_category_text.isDisable() != action)
  		compReg.frm_category_text.setDisable(action);
  	if (compReg.frm_category_select.isDisable() != action)
  		compReg.frm_category_select.setDisable(action);
  	
  	if (compReg.frm_category_select.getItems().size() > 0) {
  		if (compReg.frm_category_buttonChange.isDisable() != action)
  			compReg.frm_category_buttonChange.setDisable(action);  		
  	}
  	
  	if (compReg.frm_details_text.isDisable() != action)
  		compReg.frm_details_text.setDisable(action);
  	
  	if (compReg.btns_controls_send.isDisable() != action)
  		compReg.btns_controls_send.setDisable(action);
  	
  	if (compReg.btns_controls_clean.isDisable() != action)
  		compReg.btns_controls_clean.setDisable(action);

  }

  public static Object[] catchData () {
  	Comp_FormAgenda compReg = Comp_FormAgenda.init();
  	
  	Object [] catchs = new Object[2];
  	catchs[0] = null;
  	catchs[1] = null;
  	
    Data_Agenda agendaNew = new Data_Agenda();
    
    if (compReg.frm_category_text.isVisible()) {
    	Data_Category categoryNew = new Data_Category(-1, compReg.frm_category_text.getText());
    	
    	catchs[1] = categoryNew;
    } else
    	agendaNew.setCategory(compReg.frm_category_select.getValue().getId());
    
    agendaNew.setWeb(compReg.frm_web_text.getText());
    if (!compReg.frm_alias_text.getText().equals(""))
    	agendaNew.setAlias(compReg.frm_alias_text.getText());
  	agendaNew.setEmail(compReg.frm_email_text.getText());
  	agendaNew.setPassword(compReg.frm_password_text.getText());
  	if (!compReg.frm_details_text.getText().equals(""))
  		agendaNew.setDetails(compReg.frm_details_text.getText());
  	
  	catchs[0] = agendaNew;
  	
  	return catchs;
  }
  
  /**
   * 
   * @param listTable True para indicar que el evento proviene de la tabla. False para indicar que el evento proviene del FormAgenda
   */
  public static void changeStatusCategory (boolean listTable) {
  	Comp_FormAgenda compFormAgenda = Comp_FormAgenda.init();
  	
  	compFormAgenda.frm_category_text.clear();
		compFormAgenda.frm_category_select.setValue(null);
		
		if (listTable) {
			compFormAgenda.frm_category_select.setVisible(true);
			compFormAgenda.frm_category_text.setVisible(false);
		} else {
			if (compFormAgenda.frm_category_select.isVisible()) {
				compFormAgenda.frm_category_select.setVisible(false);
				compFormAgenda.frm_category_text.setVisible(true);
				compFormAgenda.frm_category_text.requestFocus();
			} else {
				compFormAgenda.frm_category_select.setVisible(true);
				compFormAgenda.frm_category_text.setVisible(false);
			}			
		}
  }
  
  public static void chargeCategories (Connection conn) throws Exce_DAO_Exception {
  	Comp_FormAgenda compFormAgenda = Comp_FormAgenda.init();
  	
  	ObservableList<Data_Category> listCategory = null;

  	DAO_Management manaDB = DAO_Management.init(conn);
  	listCategory = manaDB.daoCategory().read();
  	
		if (listCategory.size() > 0) {
			compFormAgenda.frm_category_select.setItems(listCategory);
			compFormAgenda.frm_category_select.setVisible(true);
			compFormAgenda.frm_category_text.setVisible(false);
			compFormAgenda.frm_category_buttonChange.setDisable(false);
		} else {
			compFormAgenda.frm_category_select.setVisible(false);
			compFormAgenda.frm_category_text.setVisible(true);
			compFormAgenda.frm_category_buttonChange.setDisable(true);
		}
  }
    
  /**
   * 
   * @param category False eliminará todas las notificaciones excepto categoría. True eliminará solo la notificacón de categoría 
   */
  public static void clearValidators (boolean category) {
  	Comp_FormAgenda compFormAgenda = Comp_FormAgenda.init();
  	
  	if (!category) {
  		compFormAgenda.val_noChanges.setText("");
  		compFormAgenda.val_noChanges.setMinHeight(0);
  		compFormAgenda.val_noChanges.setVisible(false);

  		compFormAgenda.val_web.setText("");
  		compFormAgenda.val_web.setMinHeight(0);
  		compFormAgenda.val_web.setVisible(false);

  		compFormAgenda.val_alias.setText("");
  		compFormAgenda.val_alias.setMinHeight(0);
  		compFormAgenda.val_alias.setVisible(false);

  		compFormAgenda.val_email.setText("");
  		compFormAgenda.val_email.setMinHeight(0);
  		compFormAgenda.val_email.setVisible(false);

  		compFormAgenda.val_password.setText("");
  		compFormAgenda.val_password.setMinHeight(0);
  		compFormAgenda.val_password.setVisible(false);

  		compFormAgenda.val_details.setText("");
  		compFormAgenda.val_details.setMinHeight(0);
  		compFormAgenda.val_details.setVisible(false);
  		
  	} else {
  		compFormAgenda.val_category.setText("");
  		compFormAgenda.val_category.setMinHeight(0);  		
  		compFormAgenda.val_category.setVisible(false);  		
  	}
  		
  }
  
  /**
	 * 
	 * @param status 0 para un registro nuevo. 1 para un cambio de registro. 2 Para eliminar un registro
	 */
  public static void statusForm (int status) {
  	Comp_FormAgenda compFormAgenda = Comp_FormAgenda.init();
  	compFormAgenda.setActionForm(status);
  	
  	Comp_Table compTable = Comp_Table.init();
  	
  	Tool_ReqJSON lang = null, langButtons = null;
  	
  	try {
      lang = new Tool_ReqJSON("sce", "Scene_esMx");
      langButtons = new Tool_ReqJSON("comp", "Buttons_esMx");
    } catch (IOException e) {
      e.printStackTrace();
    } catch (ParseException e) {
      e.printStackTrace();
    }
  	
  	View_Registry winReg = View_Registry.init();
  	
  	switch (status) {
  	 	case 0:
  	 		compFormAgenda.btns_controls_send.setText(langButtons.getStringJSON("btnNew"));
    		winReg.getWindow().setTitle(lang.getStringJSON("RegistryNewTitle"));
    		
    		compTable.tab_main.getSelectionModel().select(null);
  			compTable.cont_btns_edit.setDisable(true);
  			compTable.cont_btns_delete.setDisable(true); 
  	 		break;
  	 	case 1:
  	 		compFormAgenda.btns_controls_send.setText(langButtons.getStringJSON("btnUpdate"));
    		winReg.getWindow().setTitle(lang.getStringJSON("RegistryEditTitle"));
  	 		break;
  	 	case 2:
  	 		
  	 		break;
  	}
  	
  }

}