package views.registry;

import data.Data_Agenda;
import data.Data_Category;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;

/**
 *
 * Archivo Comp_FormAgenda.java
 * @author Joaquin Reyes Sanchez [Hakyn Seyer] <joaquin.seyer21@gmail.com>
 * Creado el día 28/01/19 a las 12:03 AM
 * @license This software is MIT licensed (see LICENSE)
 * 
*/

public class Comp_FormAgenda { 
	
	// Variables
	private int actionForm = 0;
	private Data_Agenda oldDataAgenda = null;
	

	// Validators
	public final Label val_noChanges = new Label();
	public final Label val_web = new Label();
	public final Label val_alias = new Label();
	public final Label val_email = new Label();
	public final Label val_password = new Label();
	public final Label val_category = new Label();
	public final Label val_details = new Label();

	// Scrolls
	public final ScrollPane scroll_frm = new ScrollPane();
	
	//Layouts
	public final VBox layout_main = new VBox();
	public final VBox layout_frm = new VBox();
	public final VBox layout_controls = new VBox();
	
	// Área Formulario
		// Grupo Web
		public final VBox frm_web = new VBox();
		public final Label frm_web_title = new Label();
		public final TextField frm_web_text = new TextField();
		
		// Grupo Alias
		public final VBox frm_alias = new VBox();
		public final Label frm_alias_title = new Label();
		public final TextField frm_alias_text = new TextField();
	
		// Grupo Email
		public final VBox frm_email = new VBox();
		public final Label frm_email_title = new Label();
		public final TextField frm_email_text = new TextField();
	
		// Grupo Password
		public final VBox frm_password = new VBox();
		public final Label frm_password_title = new Label();
		public final TextField frm_password_text = new TextField();
	
		// Grupo Categoría
		public final VBox frm_category = new VBox();
		public final HBox frm_category_box = new HBox();
		public final GridPane frm_category_grid = new GridPane();
		public final Label frm_category_title = new Label();
		public final TextField frm_category_text = new TextField();
		public final ComboBox<Data_Category> frm_category_select = new ComboBox<>();
		public final Button frm_category_buttonChange = new Button();
		
		// Grupo Detalles
		public final VBox frm_details = new VBox();
		public final Label frm_details_title = new Label();
		public final TextArea frm_details_text = new TextArea();
		
	// Área Botones
		//Grupo Controles
		public final HBox btns_controls = new HBox();
		public final  Button btns_controls_send = new Button();
		public final Button btns_controls_clean = new Button();
		public final Button btns_controls_exit = new Button();
	
		
	private static Comp_FormAgenda compRegistry = null;
		
	public static Comp_FormAgenda init () {
		if (Comp_FormAgenda.compRegistry == null)
			Comp_FormAgenda.compRegistry = new Comp_FormAgenda();
		
		return Comp_FormAgenda.compRegistry;
	}
		
	private Comp_FormAgenda () {}
	
	public int getActionForm () {
		return this.actionForm;
	}
	
	/**
	 * 
	 * @param action 0 para un registro nuevo. 1 para un cambio de registro. 2 Para eliminar un registro
	 */
	public void setActionForm (int action) {
		this.actionForm = action;
	}
	
	public Data_Agenda getOldDataAgenda () {
		return this.oldDataAgenda;
	}
	
	/**
	 * 
	 * @param agenda La agenda original del cual se pretende cambiar
	 */
	public void setOldDataAgenda (Data_Agenda agenda) {
		this.oldDataAgenda = agenda;
	}
}
