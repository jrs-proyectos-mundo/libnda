package views.registry.actions;

import java.sql.Connection;

import data.Data_Agenda;
import data.Data_Category;
import exceptions.Exce_DAO_Exception;
import inter.Inter_Actions;
import javafx.scene.control.Control;
import tools.Tool_ConnectionDB;
import tvdb.TVDB_Agenda;
import tvdb.TVDB_Category;
import views.home.mixins.Mix_Table;
import views.registry.Comp_FormAgenda;
import views.registry.mixins.Mix_FormAgenda;

/**
 *
 * Archivo Act_CategoryAgenda.java
 * @author Joaquin Reyes Sanchez [Hakyn Seyer] <joaquin.seyer21@gmail.com>
 * Creado el día 28/01/19 a las 12:02 AM
 * @license This software is MIT licensed (see LICENSE)
 * 
*/

public class Act_CategoryAgenda implements Inter_Actions {

	private Comp_FormAgenda compFormAgenda = Comp_FormAgenda.init();
	
	private Data_Agenda agenda;
	private Data_Category category;
	
	public Act_CategoryAgenda (Data_Agenda agenda) {
		this.agenda = agenda;
		
		this.runAction();
	}
	
	public Act_CategoryAgenda () {
		Object [] catchData = Mix_FormAgenda.catchData();
		
		this.agenda = (Data_Agenda) catchData[0];
		this.category = (Data_Category) catchData[1];
		
		this.runAction();
	}
	
	@Override
	public void runAction() {
		switch (this.compFormAgenda.getActionForm()) {
			case 0:
				this.newRegistry();	
				break;
			case 1:
				this.updatedRegistry();	
				break;
			case 2:
				this.deleteRegistry();
				break;
		}
	}

	@Override
	public void newRegistry() {
		Tool_ConnectionDB conn = Tool_ConnectionDB.init();
		
		try {
			if (this.category != null) {	
				// Creación de Categoría y Agenda
				TVDB_Category workingCategory = this.workingCategory(conn.getConnection(), this.category, null);
				workingCategory.Treatment();
				
				Mix_FormAgenda.clearValidators(true);
				Mix_FormAgenda.clearValidators(false);
				
				if (workingCategory.Validate()) {
					
					if (workingCategory.ValidateDataBase(0)) {
						TVDB_Agenda workingAgenda = this.workingAgenda(conn.getConnection(), this.agenda, null);
						workingAgenda.Treatment();
						
						if (workingAgenda.Validate()) {
							
							if (workingAgenda.DataBaseTransactionCreate(workingCategory.getNewData())) {
								Mix_FormAgenda.cleanForm();
								Mix_FormAgenda.chargeCategories(conn.getConnection());
								Mix_Table.currentTable();
								
								if (this.compFormAgenda.getActionForm() != 0)
									Mix_FormAgenda.statusForm(0);
							}
						}
					}
					
				}
			} else {
				// Creación Agenda
				TVDB_Agenda workingAgenda = this.workingAgenda(conn.getConnection(), this.agenda, null);
				workingAgenda.Treatment();
				
				Mix_FormAgenda.clearValidators(true);
				Mix_FormAgenda.clearValidators(false);
				
				if (workingAgenda.Validate()) {
					
					if (workingAgenda.DataBase()) {
						Mix_FormAgenda.cleanForm();
						Mix_Table.currentTable();
						
						if (this.compFormAgenda.getActionForm() != 0)
    					Mix_FormAgenda.statusForm(0);
					}
				}
			}
			
		} catch (Exce_DAO_Exception e) {
			e.printStackTrace();
		} finally {
			conn.closeConnection();
		}
	}

	@Override
	public void updatedRegistry() {
		Tool_ConnectionDB conn = Tool_ConnectionDB.init();
		
		try {
			if (this.category != null) {
				// Creación Categoría y Edición de la Agenda
				TVDB_Category workingCategory = this.workingCategory(conn.getConnection(), this.category, null);
				workingCategory.Treatment();
				
				Mix_FormAgenda.clearValidators(true);
				Mix_FormAgenda.clearValidators(false);
				
				if (workingCategory.Validate()) {
					
					if (workingCategory.ValidateDataBase(0)) {
						TVDB_Agenda workingAgenda = this.workingAgenda(conn.getConnection(), this.agenda, this.compFormAgenda.getOldDataAgenda());
						workingAgenda.Treatment();
						
						if (workingAgenda.ValidateUpdate()) {
							
							if (workingAgenda.Validate()) {
								
								if (workingAgenda.DataBaseTransactionUpdate(workingCategory.getNewData())) {
									Mix_FormAgenda.cleanForm();
									Mix_FormAgenda.chargeCategories(conn.getConnection());
									Mix_Table.currentTable();
									
									if (this.compFormAgenda.getActionForm() != 0)
			    					Mix_FormAgenda.statusForm(0);
								}
							}
						}
					}
					
				}
				
			} else {
				// Edicion Agenda
				TVDB_Agenda workingAgenda = this.workingAgenda(conn.getConnection(), this.agenda, this.compFormAgenda.getOldDataAgenda());
				workingAgenda.Treatment();
				
				Mix_FormAgenda.clearValidators(true);
				Mix_FormAgenda.clearValidators(false);
				
				if (workingAgenda.ValidateUpdate()) {

					if (workingAgenda.Validate()) {
						
						if (workingAgenda.DataBaseUpdate()) {
							Mix_FormAgenda.cleanForm();
							Mix_Table.currentTable();
							
							if (this.compFormAgenda.getActionForm() != 0)
	    					Mix_FormAgenda.statusForm(0);
						}
					}
				}
			}
		} catch (Exce_DAO_Exception e) {
			e.printStackTrace();
		} finally {
			conn.closeConnection();
		}
	}
	

	@Override
	public void deleteRegistry() {
		Tool_ConnectionDB conn = Tool_ConnectionDB.init();
		
		try {
			TVDB_Agenda workingAgenda = this.workingAgenda(conn.getConnection(), null, this.agenda);
			
			Mix_FormAgenda.clearValidators(true);
			Mix_FormAgenda.clearValidators(false);
			
			if (workingAgenda.DataBaseDelete()) {
				Mix_Table.currentTable();
				
				if (this.compFormAgenda.getActionForm() != 0)
					Mix_FormAgenda.statusForm(0);
			}
		} catch (Exce_DAO_Exception e) {
			e.printStackTrace();
		} finally {
			conn.closeConnection();
		}
	}
	
	private TVDB_Category workingCategory (Connection conn, Data_Category newCategory, Data_Category oldCategory) {
		TVDB_Category workingCategory = new TVDB_Category(conn, newCategory, oldCategory) {
			
			@Override
			public void noticeNoChanges(String error) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void noticeName(String error) {
				Act_CategoryAgenda.this.compFormAgenda.val_category.setMinHeight(Control.USE_PREF_SIZE);
				Act_CategoryAgenda.this.compFormAgenda.val_category.setText(error);				
				Act_CategoryAgenda.this.compFormAgenda.val_category.setVisible(true);				
			}

			@Override
			public void clearNoticeNoChanges() {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void clearNoticeName() {
				Act_CategoryAgenda.this.compFormAgenda.val_category.setMinHeight(0);
				Act_CategoryAgenda.this.compFormAgenda.val_category.setText("");
				Act_CategoryAgenda.this.compFormAgenda.val_category.setVisible(false);
			}
		};
		
		return workingCategory;
	}
	
	private TVDB_Agenda workingAgenda (Connection conn, Data_Agenda newAgenda, Data_Agenda oldAgenda) {
		TVDB_Agenda workingAgenda = new TVDB_Agenda(conn, newAgenda, oldAgenda) {
			
			@Override
			public void noticeNoChanges(String error) {
				Act_CategoryAgenda.this.compFormAgenda.val_noChanges.setMinHeight(Control.USE_PREF_SIZE);
				Act_CategoryAgenda.this.compFormAgenda.val_noChanges.setText(error);				
				Act_CategoryAgenda.this.compFormAgenda.val_noChanges.setVisible(true);
			}
			
			@Override
			public void noticeWeb(String error) {
				Act_CategoryAgenda.this.compFormAgenda.val_web.setMinHeight(Control.USE_PREF_SIZE);
				Act_CategoryAgenda.this.compFormAgenda.val_web.setText(error);				
				Act_CategoryAgenda.this.compFormAgenda.val_web.setVisible(true);				
			}
			
			@Override
			public void noticeAlias(String error) {
				Act_CategoryAgenda.this.compFormAgenda.val_alias.setMinHeight(Control.USE_PREF_SIZE);
				Act_CategoryAgenda.this.compFormAgenda.val_alias.setText(error);
				Act_CategoryAgenda.this.compFormAgenda.val_alias.setVisible(true);
			}
			
			@Override
			public void noticeEmail(String error) {
				Act_CategoryAgenda.this.compFormAgenda.val_email.setMinHeight(Control.USE_PREF_SIZE);
				Act_CategoryAgenda.this.compFormAgenda.val_email.setText(error);				
				Act_CategoryAgenda.this.compFormAgenda.val_email.setVisible(true);				
			}
			
			@Override
			public void noticePassword(String error) {
				Act_CategoryAgenda.this.compFormAgenda.val_password.setMinHeight(Control.USE_PREF_SIZE);
				Act_CategoryAgenda.this.compFormAgenda.val_password.setText(error);				
				Act_CategoryAgenda.this.compFormAgenda.val_password.setVisible(true);				
			}
			
			@Override
			public void noticeDetails(String error) {
				Act_CategoryAgenda.this.compFormAgenda.val_details.setMinHeight(Control.USE_PREF_SIZE);
				Act_CategoryAgenda.this.compFormAgenda.val_details.setText(error);				
				Act_CategoryAgenda.this.compFormAgenda.val_details.setVisible(true);				
			}

			@Override
			public void clearNoticeNoChanges() {
				Act_CategoryAgenda.this.compFormAgenda.val_noChanges.setMinHeight(0);
				Act_CategoryAgenda.this.compFormAgenda.val_noChanges.setText("");
				Act_CategoryAgenda.this.compFormAgenda.val_noChanges.setVisible(false);
			}

			@Override
			public void clearNoticeWeb() {
				Act_CategoryAgenda.this.compFormAgenda.val_web.setMinHeight(0);
				Act_CategoryAgenda.this.compFormAgenda.val_web.setText("");				
				Act_CategoryAgenda.this.compFormAgenda.val_web.setVisible(false);				
			}

			@Override
			public void clearNoticeAlias() {
				Act_CategoryAgenda.this.compFormAgenda.val_alias.setMinHeight(0);
				Act_CategoryAgenda.this.compFormAgenda.val_alias.setText("");
				Act_CategoryAgenda.this.compFormAgenda.val_alias.setVisible(false);
			}

			@Override
			public void clearNoticeEmail() {
				Act_CategoryAgenda.this.compFormAgenda.val_email.setMinHeight(0);
				Act_CategoryAgenda.this.compFormAgenda.val_email.setText("");
				Act_CategoryAgenda.this.compFormAgenda.val_email.setVisible(false);
			}

			@Override
			public void clearNoticePassword() {
				Act_CategoryAgenda.this.compFormAgenda.val_password.setMinHeight(0);
				Act_CategoryAgenda.this.compFormAgenda.val_password.setText("");
				Act_CategoryAgenda.this.compFormAgenda.val_password.setVisible(false);
			}

			@Override
			public void clearNoticeDetails() {
				Act_CategoryAgenda.this.compFormAgenda.val_details.setMinHeight(0);
				Act_CategoryAgenda.this.compFormAgenda.val_details.setText("");
				Act_CategoryAgenda.this.compFormAgenda.val_details.setVisible(false);
			}
		};
		
		return workingAgenda;
	}
		
}
