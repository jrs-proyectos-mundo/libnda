package views.registry.scenes;

import dao.DAO_Management;
import data.Data_Category;
import exceptions.Exce_DAO_Exception;
import inter.Inter_Scenes;
import javafx.collections.ObservableList;
import javafx.scene.Scene;
import javafx.scene.image.ImageView;
import javafx.util.StringConverter;
import mixins.Mix_Lang;
import tools.Tool_ConnectionDB;
import tools.Tool_Image;
import tools.Tool_ReqJSON;
import views.registry.Comp_FormAgenda;

/**
 *
 * Archivo Sce_FormAgenda.java
 * @author Joaquin Reyes Sanchez [Hakyn Seyer] <joaquin.seyer21@gmail.com>
 * Creado el día 28/01/19 a las 12:03 AM
 * @license This software is MIT licensed (see LICENSE)
 * 
*/

public class Sce_FormAgenda implements Inter_Scenes {

	private static Sce_FormAgenda sceneFormAgenda = null;
		
	private double regWidth = 0, regHeight = 0;
	private int compSpacing = 5, compHeight = 25;
	
	private String classGroup = "form__group", classGroupLabel = "form__group__label", classGroupTextField = "form__group__text-field", classGroupButtons="form__group__button";
	
	private Tool_ReqJSON dataAgendaLang = null, buttonsLang = null;
	
	private Scene regScene = null;
	
	private final Comp_FormAgenda compFormAgenda = Comp_FormAgenda.init();
	
	/**
	 * 
	 * @param regWidth Anchura de la ventana registry
	 * @param regHeight Altura de la ventana registry
	 * @return Instancía Sce_FormAgenda
	 */
	public static Sce_FormAgenda init (double regWidth, double regHeight) {
		
		if (Sce_FormAgenda.sceneFormAgenda == null)
			Sce_FormAgenda.sceneFormAgenda = new Sce_FormAgenda(regWidth, regHeight);
		
		return Sce_FormAgenda.sceneFormAgenda;
		
	}
	
	private Sce_FormAgenda (double regWidth, double regHeight) {
		this.regWidth = regWidth;
		this.regHeight = regHeight;
		
		this.dataAgendaLang = Mix_Lang.getlang("data", "Agenda_esMx");
		this.buttonsLang = Mix_Lang.getlang("comp", "Buttons_esMx");
	}

	@Override
	public Scene buildingScene() {
		if (this.regScene == null) {
			this.compFormAgenda.layout_main.setPrefSize(this.regWidth, this.regHeight);
			this.compFormAgenda.layout_main.getStyleClass().add("window");
			
			this.compFormAgenda.scroll_frm.setPrefWidth(this.regWidth);
			this.compFormAgenda.scroll_frm.getStyleClass().add("scroll");
			this.compFormAgenda.scroll_frm.setFitToWidth(true);
//			this.compFormAgenda.scroll_frm.setFitToHeight(true);
			
			this.compFormAgenda.layout_frm.getStyleClass().add("form");
			this.compFormAgenda.layout_frm.setPrefSize(this.regWidth, this.regHeight);
			
			this.formWeb();
			this.formAlias();
			this.formEmail();
			this.formPassword();
			this.formCategory();
			this.formDetails();
			
			this.compFormAgenda.layout_frm.getChildren().addAll(
					this.compFormAgenda.frm_web,
					this.compFormAgenda.frm_alias,
					this.compFormAgenda.frm_email,
					this.compFormAgenda.frm_password,
					this.compFormAgenda.frm_category,
					this.compFormAgenda.frm_details
			);
			
			this.compFormAgenda.scroll_frm.setContent(this.compFormAgenda.layout_frm);

			this.buttonsControls();
			
			this.compFormAgenda.val_noChanges.setWrapText(true);
	    this.compFormAgenda.val_noChanges.getStyleClass().addAll("validator", "validator--main");
	    this.compFormAgenda.val_noChanges.setPrefWidth(this.regWidth);
	    this.compFormAgenda.val_noChanges.setMinHeight(0);
	    this.compFormAgenda.val_noChanges.setVisible(false);
			
			this.compFormAgenda.layout_main.getChildren().addAll(
					this.compFormAgenda.val_noChanges,
					this.compFormAgenda.scroll_frm,
					this.compFormAgenda.layout_controls
					);
			
			this.regScene = new Scene (this.compFormAgenda.layout_main, this.regWidth, this.regHeight);
			this.regScene.getStylesheets().add(this.getClass().getResource("/styles/sce/Registry_Form.css").toString());
		}
		
		return this.regScene;
	}
	
	private void formWeb () {
		this.compFormAgenda.frm_web.setSpacing(this.compSpacing);
		this.compFormAgenda.frm_web.getStyleClass().add(this.classGroup);
		this.compFormAgenda.frm_web.setPrefWidth(this.regWidth);
    
		this.compFormAgenda.frm_web_title.setText(this.dataAgendaLang.getStringJSON("webTitle"));
    this.compFormAgenda.frm_web_title.getStyleClass().add(this.classGroupLabel);
    this.compFormAgenda.frm_web_title.setPrefSize(this.regWidth, this.compHeight);
    
    this.compFormAgenda.frm_web_text.getStyleClass().add(this.classGroupTextField);
    this.compFormAgenda.frm_web_text.setPrefSize(this.regWidth, this.compHeight);
    
    this.compFormAgenda.val_web.setWrapText(true);
    this.compFormAgenda.val_web.getStyleClass().add("validator");
    this.compFormAgenda.val_web.setPrefWidth(this.regWidth);
//    this.compFormAgenda.val_web.setMinHeight(Control.USE_PREF_SIZE);
    this.compFormAgenda.val_web.setMinHeight(0);
    this.compFormAgenda.val_web.setVisible(false);
   
    this.compFormAgenda.frm_web.getChildren().addAll(
    		this.compFormAgenda.frm_web_title,
    		this.compFormAgenda.frm_web_text,
    		this.compFormAgenda.val_web
		);
	}
	
	private void formAlias () {
		this.compFormAgenda.frm_alias.setSpacing(this.compSpacing);
		this.compFormAgenda.frm_alias.getStyleClass().add(this.classGroup);
		this.compFormAgenda.frm_alias.setPrefWidth(this.regWidth);
		
		this.compFormAgenda.frm_alias_title.setText(this.dataAgendaLang.getStringJSON("aliasTitle"));
		this.compFormAgenda.frm_alias_title.getStyleClass().add(this.classGroupLabel);
		this.compFormAgenda.frm_alias_title.setPrefSize(this.regWidth, this.compHeight);
		
		this.compFormAgenda.frm_alias_text.getStyleClass().add(this.classGroupTextField);
		this.compFormAgenda.frm_alias_text.setPrefSize(this.regWidth, this.compHeight);
		
		this.compFormAgenda.val_alias.setWrapText(true);
    this.compFormAgenda.val_alias.getStyleClass().add("validator");
    this.compFormAgenda.val_alias.setPrefWidth(this.regWidth);
    this.compFormAgenda.val_alias.setMinHeight(0);
    this.compFormAgenda.val_alias.setVisible(false);
		
		this.compFormAgenda.frm_alias.getChildren().addAll(
				this.compFormAgenda.frm_alias_title,
				this.compFormAgenda.frm_alias_text,
				this.compFormAgenda.val_alias
		);
	}
	
	private void formEmail () {
		this.compFormAgenda.frm_email.setSpacing(this.compSpacing);
		this.compFormAgenda.frm_email.getStyleClass().add(this.classGroup);
		this.compFormAgenda.frm_email.setPrefWidth(this.regWidth);
		
		this.compFormAgenda.frm_email_title.setText(this.dataAgendaLang.getStringJSON("emailTitle"));
		this.compFormAgenda.frm_email_title.getStyleClass().add(this.classGroupLabel);
		this.compFormAgenda.frm_email_title.setPrefSize(this.regWidth, this.compHeight);
		
		this.compFormAgenda.frm_email_text.getStyleClass().add(this.classGroupTextField);
		this.compFormAgenda.frm_email_text.setPrefSize(this.regWidth, this.compHeight);
		
		this.compFormAgenda.val_email.setWrapText(true);
    this.compFormAgenda.val_email.getStyleClass().add("validator");
    this.compFormAgenda.val_email.setPrefWidth(this.regWidth);
    this.compFormAgenda.val_email.setMinHeight(0);
    this.compFormAgenda.val_email.setVisible(false);
		
		this.compFormAgenda.frm_email.getChildren().addAll(
				this.compFormAgenda.frm_email_title,
				this.compFormAgenda.frm_email_text,
				this.compFormAgenda.val_email
		);
	}
	
	private void formPassword () {
		this.compFormAgenda.frm_password.setSpacing(this.compSpacing);
		this.compFormAgenda.frm_password.getStyleClass().add(this.classGroup);
		this.compFormAgenda.frm_password.setPrefWidth(this.regWidth);
		
		this.compFormAgenda.frm_password_title.setText(this.dataAgendaLang.getStringJSON("passwordTitle"));
		this.compFormAgenda.frm_password_title.getStyleClass().add(this.classGroupLabel);
		this.compFormAgenda.frm_password_title.setPrefSize(this.regWidth, this.compHeight);
		
		this.compFormAgenda.frm_password_text.getStyleClass().add(this.classGroupTextField);
		this.compFormAgenda.frm_password_text.setPrefSize(this.regWidth, this.compHeight);
		
		this.compFormAgenda.val_password.setWrapText(true);
    this.compFormAgenda.val_password.getStyleClass().add("validator");
    this.compFormAgenda.val_password.setPrefWidth(this.regWidth);
    this.compFormAgenda.val_password.setMinHeight(0);
    this.compFormAgenda.val_password.setVisible(false);
		
		this.compFormAgenda.frm_password.getChildren().addAll(
				this.compFormAgenda.frm_password_title,
				this.compFormAgenda.frm_password_text,
				this.compFormAgenda.val_password
		);
	}
	
	private void formCategory () {
		this.compFormAgenda.frm_category.setSpacing(this.compSpacing);
		this.compFormAgenda.frm_category.setPrefWidth(this.regWidth);
		
		this.compFormAgenda.frm_category_box.setSpacing(this.compSpacing);
		this.compFormAgenda.frm_category_box.getStyleClass().add(this.classGroup);
		this.compFormAgenda.frm_category_box.setPrefWidth(this.regWidth);
		
		this.compFormAgenda.frm_category_grid.setHgap(this.compSpacing);
		this.compFormAgenda.frm_category_grid.setVgap(this.compSpacing);
		this.compFormAgenda.frm_category_grid.getStyleClass().add("form__group__box");
		this.compFormAgenda.frm_category_grid.setPrefWidth(this.regWidth);
		
		this.compFormAgenda.frm_category_title.setText(this.dataAgendaLang.getStringJSON("categoryTitle"));
		this.compFormAgenda.frm_category_title.getStyleClass().add("form__group__box__label");
		this.compFormAgenda.frm_category_title.setPrefSize(this.regWidth, this.compHeight);
		this.compFormAgenda.frm_category_grid.add(this.compFormAgenda.frm_category_title, 0, 0, 2, 1);
		
		this.compFormAgenda.frm_category_text.getStyleClass().add(this.classGroupTextField);
		this.compFormAgenda.frm_category_text.setPrefSize(this.regWidth * .75, this.compHeight);
		this.compFormAgenda.frm_category_text.setVisible(false);
		this.compFormAgenda.frm_category_grid.add(this.compFormAgenda.frm_category_text, 0, 1);
		
		this.compFormAgenda.frm_category_select.getStyleClass().add("form__group__box__combobox");
		this.compFormAgenda.frm_category_select.setPrefSize(this.regWidth * .75, this.compHeight);
		this.compFormAgenda.frm_category_select.setPromptText(this.dataAgendaLang.getStringJSON("categoryPrompt"));
		this.compFormAgenda.frm_category_grid.add(this.compFormAgenda.frm_category_select, 0, 1);
		
//		this.compFormAgenda.frm_category_buttonChange.setText(this.buttonsLang.getStringJSON("btnNew"));
		this.compFormAgenda.frm_category_buttonChange.setGraphic(new ImageView(Tool_Image.getImage("app", "nuevo", "png")));
		this.compFormAgenda.frm_category_buttonChange.getStyleClass().add("form__group__box__button");
		this.compFormAgenda.frm_category_buttonChange.setPrefSize(this.regWidth * .25, this.compHeight);
		this.compFormAgenda.frm_category_grid.add(this.compFormAgenda.frm_category_buttonChange, 1, 1);
		
		
		this.compFormAgenda.frm_category_box.getChildren().addAll(
			this.compFormAgenda.frm_category_grid
		);
		
		this.compFormAgenda.val_category.setWrapText(true);
		this.compFormAgenda.val_category.getStyleClass().add("validator");
		this.compFormAgenda.val_category.setPrefWidth(this.regWidth);
		this.compFormAgenda.val_category.setMinHeight(0);
		this.compFormAgenda.val_category.setVisible(false);
		
		this.compFormAgenda.frm_category.getChildren().addAll(
				this.compFormAgenda.frm_category_box,
				this.compFormAgenda.val_category
				);
		
		ObservableList<Data_Category> categoryList = null;
		
		Tool_ConnectionDB conn = Tool_ConnectionDB.init();
		
		try {
			DAO_Management manaDB = DAO_Management.init(conn.getConnection());
			
			categoryList = manaDB.daoCategory().read();
		} catch (Exce_DAO_Exception e ) {
			e.printStackTrace();
		} finally {
			conn.closeConnection();
		}
		
		if (categoryList.size() > 0) {
			this.compFormAgenda.frm_category_select.setItems(categoryList);
			this.compFormAgenda.frm_category_select.setVisible(true);
			this.compFormAgenda.frm_category_text.setVisible(false);
			this.compFormAgenda.frm_category_buttonChange.setDisable(false);			
		} else {
			this.compFormAgenda.frm_category_select.setVisible(false);
			this.compFormAgenda.frm_category_text.setVisible(true);
			this.compFormAgenda.frm_category_buttonChange.setDisable(true);
		}
		
		this.compFormAgenda.frm_category_select.setConverter(new StringConverter<Data_Category>() {
			
			@Override
			public String toString(Data_Category object) {
				if (object != null)
					return object.getName();
				
				return "";
			}
			
			@Override
			public Data_Category fromString(String string) {
				if (string != null) {
					for (Data_Category categoryItem : Sce_FormAgenda.this.compFormAgenda.frm_category_select.getItems()) {
						if (categoryItem.getName().equals(string))
							return categoryItem;
					}
				}
				
				return null;
			}
		});
		
	}
	
	private void formDetails () {
		this.compFormAgenda.frm_details.setSpacing(this.compSpacing);
		this.compFormAgenda.frm_details.getStyleClass().add(this.classGroup);
		this.compFormAgenda.frm_details.setPrefWidth(this.regWidth);
		
		this.compFormAgenda.frm_details_title.setText(this.dataAgendaLang.getStringJSON("detailsTitle"));
		this.compFormAgenda.frm_details_title.getStyleClass().add(this.classGroupLabel);
		this.compFormAgenda.frm_details_title.setPrefSize(this.regWidth, this.compHeight);
		
		this.compFormAgenda.frm_details_text.getStyleClass().addAll(this.classGroupTextField, "form__group__text-area");
		this.compFormAgenda.frm_details_text.setPrefWidth(this.regWidth);
		this.compFormAgenda.frm_details_text.setPrefRowCount(5);
		this.compFormAgenda.frm_details_text.setWrapText(true);
		
		this.compFormAgenda.val_details.setWrapText(true);
    this.compFormAgenda.val_details.getStyleClass().add("validator");
    this.compFormAgenda.val_details.setPrefWidth(this.regWidth);
    this.compFormAgenda.val_details.setMinHeight(0);
    this.compFormAgenda.val_details.setVisible(false);
		
		this.compFormAgenda.frm_details.getChildren().addAll(
				this.compFormAgenda.frm_details_title,
				this.compFormAgenda.frm_details_text,
				this.compFormAgenda.val_details
		);
	}
	
	private void buttonsControls () {
		this.compFormAgenda.layout_controls.getStyleClass().add("button-controls");
		this.compFormAgenda.layout_controls.setPrefWidth(this.regWidth);
		
		this.compFormAgenda.btns_controls.setSpacing(this.compSpacing);
		this.compFormAgenda.btns_controls.getStyleClass().add(this.classGroup);
		
		this.compFormAgenda.btns_controls_send.setText(this.buttonsLang.getStringJSON("btnNew"));
		this.compFormAgenda.btns_controls_send.getStyleClass().addAll(this.classGroupButtons, "form__group__button--send");
		this.compFormAgenda.btns_controls_send.setPrefSize(this.regWidth * .50, this.compHeight * 2);
		
//		this.compFormAgenda.btns_controls_clean.setText(this.buttonsLang.getStringJSON("btnClean"));
		this.compFormAgenda.btns_controls_clean.setGraphic(new ImageView(Tool_Image.getImage("app", "limpiar", "png")));
		this.compFormAgenda.btns_controls_clean.getStyleClass().addAll(this.classGroupButtons, "form__group__button--clean");
		this.compFormAgenda.btns_controls_clean.setPrefSize(this.regWidth * .25, this.compHeight * 2);
		
//		this.compFormAgenda.btns_controls_exit.setText(this.buttonsLang.getStringJSON("btnExit"));
		this.compFormAgenda.btns_controls_exit.setGraphic(new ImageView(Tool_Image.getImage("app", "salir", "png")));
		this.compFormAgenda.btns_controls_exit.getStyleClass().addAll(this.classGroupButtons, "form__group__button--exit");
		this.compFormAgenda.btns_controls_exit.setPrefSize(this.regWidth * .25, this.compHeight * 2);
		
		this.compFormAgenda.btns_controls.getChildren().addAll(
				this.compFormAgenda.btns_controls_send,
				this.compFormAgenda.btns_controls_clean,
				this.compFormAgenda.btns_controls_exit
		);
		
		this.compFormAgenda.layout_controls.getChildren().add(this.compFormAgenda.btns_controls);
	}
	
}