package views.registry.listeners;

import java.io.IOException;

import org.json.simple.parser.ParseException;

import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import mixins.Mix_Components;
import tools.Tool_ReqJSON;
import views.View_Home;
import views.View_Registry;
import views.home.mixins.Mix_Board;
import views.registry.Comp_FormAgenda;
import views.registry.actions.Act_CategoryAgenda;
import views.registry.mixins.Mix_FormAgenda;

/**
 *
 * Archivo List_FormAgenda.java
 * @author Joaquin Reyes Sanchez [Hakyn Seyer] <joaquin.seyer21@gmail.com>
 * Creado el día 28/01/19 a las 12:02 AM
 * @license This software is MIT licensed (see LICENSE)
 * 
*/

public class List_FormAgenda {

  private static List_FormAgenda listForm = null;
  
  private Tool_ReqJSON regLang = null;
  
  private Comp_FormAgenda compFormAgenda = Comp_FormAgenda.init();

  /**
   * 
   * @return Instancía única del listener
   */
  public static List_FormAgenda init () {
    if (List_FormAgenda.listForm == null)
      List_FormAgenda.listForm = new List_FormAgenda();

    return List_FormAgenda.listForm;
  }

  private List_FormAgenda () {
  	try {
      this.regLang = new Tool_ReqJSON("comp", "Alert_esMx");
    } catch (IOException e) {
      e.printStackTrace();
    } catch (ParseException e) {
      e.printStackTrace();
    }
  	
    new List_Category();
    new List_Buttons();
  }

  private class List_Category {

    private List_Category () {
      List_FormAgenda
      	.this
      	.compFormAgenda
      	.frm_category_buttonChange
      	.setOnAction(e -> {
      		Mix_FormAgenda.changeStatusCategory(false);
      	});
    }

  }

  private class List_Buttons {

    private List_Buttons () {
      this.btnExit();
      this.btnClean();
      this.btnAdd();
    }

    private void btnAdd () {
    	List_FormAgenda
    		.this
    		.compFormAgenda
    		.btns_controls_send
    		.setOnAction(e -> {
    			Mix_FormAgenda.disabledEnableElements(true);
    			
    			if (Mix_FormAgenda.completeDataForm(false) == 4) {
    				new Act_CategoryAgenda();
    				
    				Mix_FormAgenda.disabledEnableElements(false);
    			} else {
    				Alert alert = Mix_Components.alertComponent(
              AlertType.ERROR, 
              350, 
              150, 
              List_FormAgenda.this.regLang.getStringJSON("alertIncompleteDataFormTitle"),
              List_FormAgenda.this.regLang.getStringJSON("alertIncompleteDataFormHeader"),
              List_FormAgenda.this.regLang.getStringJSON("alertIncompleteDataFormContent")
            );

            View_Registry winReg = View_Registry.init();

            alert.initOwner(winReg.getWindow());
            alert.show();

            Mix_FormAgenda.disabledEnableElements(false);
            Mix_FormAgenda.clearValidators(true);
            Mix_FormAgenda.clearValidators(false);
    			}
    		});
    }

    private void btnExit () {
    	List_FormAgenda
    		.this
    		.compFormAgenda
    		.btns_controls_exit
    		.setOnAction(e -> {
    			Mix_FormAgenda.returnWinDefaultHome(true);
    			
    			View_Home winHome = View_Home.init(null);
    			
    			Mix_Board.refreshResume(winHome.getWindow().getWidth(), winHome.getWindow().getHeight(), null);
    			Mix_Board.enableDisableControls();
    		});
    }

    private void btnClean () {
      List_FormAgenda
      	.this
      	.compFormAgenda
      	.btns_controls_clean
      	.setOnAction(e -> {
      		Mix_FormAgenda.cleanForm();
      		Mix_FormAgenda.clearValidators(true);
      		Mix_FormAgenda.clearValidators(false);
      	});
    }

  }

}