package views.category;

import data.Data_Category;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;

/**
 *
 * Archivo Comp_FormCategory.java
 * @author Joaquin Reyes Sanchez [Hakyn Seyer] <joaquin.seyer21@gmail.com>
 * Creado el día 28/01/19 a las 11:59 PM
 * @license This software is MIT licensed (see LICENSE)
 * 
*/

public class Comp_FormCategory {
	// Variables
	private int actionForm = 0;
	private Data_Category oldDataCategory = null;
	
	// Validators
	public final Label val_noChanges = new Label();
	public final Label val_name = new Label();
	
	// Scrolls
	public final ScrollPane scroll_frm = new ScrollPane();
	
	// Main Layout
	public final VBox layout_main = new VBox(); 
	public final VBox layout_control = new VBox();
	public final VBox layout_frm = new VBox();
	
	// Área Fórmulario
		//Grupo Categoria
		public final Label frm_cat_title = new Label();
		public final ComboBox<Data_Category> frm_cat_select = new ComboBox<>();
		public final TextField frm_cat_text = new TextField();
		
	// Área envio
		public final HBox frm_controls = new HBox();
		public final Button frm_btn_edit = new Button();
		public final Button frm_btn_delete = new Button();
		public final Button frm_btn_exit = new Button();
		
	private static Comp_FormCategory compCategory = null;
	
	public static Comp_FormCategory init () {
		if (Comp_FormCategory.compCategory == null)
			Comp_FormCategory.compCategory = new Comp_FormCategory();
		
		return Comp_FormCategory.compCategory;
	}
	
	private Comp_FormCategory () {}
	
	public Data_Category getOldDataCategory () {
		return this.oldDataCategory;
	}
	
	public void setOldDataCategory (Data_Category category) {
		this.oldDataCategory = category;
	}
	
	public int getActionForm () {
		return this.actionForm;
	}
	
	/**
	 * 
	 * @param action "0" para nuevo registro. "1" para editar registro. "2" para eliminar registro
	 */
	public void setActionForm (int action) {
		this.actionForm = action;
	}
		
}
