package views.category.listeners;

import java.io.IOException;

import org.json.simple.parser.ParseException;

import data.Data_Category;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import mixins.Mix_Components;
import tools.Tool_ReqJSON;
import views.View_Category;
import views.category.Comp_FormCategory;
import views.category.actions.Act_Category;
import views.category.mixins.Mix_FormCategory;

/**
 *
 * Archivo List_FormCategory.java
 * @author Joaquin Reyes Sanchez [Hakyn Seyer] <joaquin.seyer21@gmail.com>
 * Creado el día 28/01/19 a las 11:58 PM
 * @license This software is MIT licensed (see LICENSE)
 * 
*/

public class List_FormCategory {

	private static List_FormCategory listFormCategoy = null;
	
	private Tool_ReqJSON catLang = null;
	
	private Comp_FormCategory compFormCategory = Comp_FormCategory.init();
	
	public static List_FormCategory init () {
		if (List_FormCategory.listFormCategoy == null)
			List_FormCategory.listFormCategoy = new List_FormCategory();
		
		return List_FormCategory.listFormCategoy;
	}
	
	private List_FormCategory () {
		try {
			this.catLang = new Tool_ReqJSON("comp", "Alert_esMx");
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ParseException e) {
			e.printStackTrace();
		}
		
		new List_Form();
		new List_Control();
	}
	
	private class List_Form {
		private List_Form () {
			this.select();
			this.text();
		}
		
		private void text () {
			List_FormCategory
				.this
				.compFormCategory
				.frm_cat_text
				.setOnKeyPressed(e -> {
					if (List_FormCategory.this.compFormCategory.frm_cat_text.getText().isEmpty()) {
						List_FormCategory.this.compFormCategory.frm_cat_text.setDisable(true);
						List_FormCategory.this.compFormCategory.frm_cat_select.setValue(null);						
						List_FormCategory.this.compFormCategory.frm_btn_edit.setDisable(true);
						List_FormCategory.this.compFormCategory.setOldDataCategory(null);
					}
				});
		}
		
		private void select () {
			List_FormCategory
				.this
				.compFormCategory
				.frm_cat_select
				.valueProperty().addListener((obs, oldItem, newItem) -> {
					if (newItem != null) {
						List_FormCategory.this.compFormCategory.frm_cat_text.setText(newItem.getName());
						List_FormCategory.this.compFormCategory.frm_cat_text.requestFocus();
						List_FormCategory.this.compFormCategory.frm_cat_text.setDisable(false);
						List_FormCategory.this.compFormCategory.frm_btn_edit.setDisable(false);
						List_FormCategory.this.compFormCategory.frm_btn_delete.setDisable(false);
						
						Data_Category oldCategory = new Data_Category(
								newItem.getId(),
								newItem.getName()
								);
						
						List_FormCategory.this.compFormCategory.setOldDataCategory(oldCategory);
					}
				});
		}
	}
	
	private class List_Control {
		
		private List_Control () {
			this.edit();
			this.delete();
			this.exit();
		}
		
		private void edit () {
			List_FormCategory
				.this
				.compFormCategory
				.frm_btn_edit
				.setOnAction(e -> {
					List_FormCategory.this.compFormCategory.setActionForm(1);
					
					Mix_FormCategory.disabledEnableElements(true);
					
					if (Mix_FormCategory.completeDataForm(true) == 1) {
						new Act_Category();
						
						Mix_FormCategory.disabledEnableElements(false);
					} else {
						Alert alert = Mix_Components.alertComponent(
								AlertType.ERROR, 
								350, 
								150, 
								List_FormCategory.this.catLang.getStringJSON("alertIncompleteDataFormTitle"), 
								List_FormCategory.this.catLang.getStringJSON("alertIncompleteDataFormHeader"), 
								List_FormCategory.this.catLang.getStringJSON("alertIncompleteDataFormContent"));
						
						View_Category winCate = View_Category.init();
						
						alert.initOwner(winCate.getWindow());
						alert.show();
						
						Mix_FormCategory.disabledEnableElements(false);
						Mix_FormCategory.clearValidators();
					}
				});
		}
		
		public void delete () {
			List_FormCategory
				.this
				.compFormCategory
				.frm_btn_delete
				.setOnAction(e -> {
					List_FormCategory.this.compFormCategory.setActionForm(2);
					
					new Act_Category();
					
					Mix_FormCategory.disabledEnableElements(false);
				});
		}
		
		public void exit () {
			List_FormCategory
				.this
				.compFormCategory
				.frm_btn_exit
				.setOnAction(e -> {
					Mix_FormCategory.closeWindow();
				});
		}
		
	}
	
}
