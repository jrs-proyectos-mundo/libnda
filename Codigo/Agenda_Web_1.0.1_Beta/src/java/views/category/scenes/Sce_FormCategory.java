package views.category.scenes;

import dao.DAO_Management;
import javafx.collections.ObservableList;
import javafx.scene.Scene;
import javafx.scene.image.ImageView;
import javafx.util.StringConverter;
import mixins.Mix_Lang;
import tools.Tool_ConnectionDB;
import tools.Tool_Image;
import tools.Tool_ReqJSON;
import views.category.Comp_FormCategory;
import data.Data_Category;
import exceptions.Exce_DAO_Exception;
import inter.Inter_Scenes;

/**
 *
 * Archivo Sce_FormCategory.java
 * @author Joaquin Reyes Sanchez [Hakyn Seyer] <joaquin.seyer21@gmail.com>
 * Creado el día 28/01/19 a las 11:59 PM
 * @license This software is MIT licensed (see LICENSE)
 * 
*/

public class Sce_FormCategory implements Inter_Scenes{

	private static Sce_FormCategory sceneFormCategory = null;
	
	private double catWidth = 0, catHeight = 0;
	private int compHeight = 25;
	
	private Tool_ReqJSON dataAgendaLang = null;
	
	private Scene catScene = null;
	
	private final Comp_FormCategory compFormCategory = Comp_FormCategory.init();
	
	/**
	 * 
	 * @param catWidth Anchura de la ventana categoria
	 * @param catHeight Altura de la ventana categoria
	 * @return Instancía Sce_FormCategory
	 */
	public static Sce_FormCategory init (double catWidth, double catHeight) {
		if (Sce_FormCategory.sceneFormCategory == null)
			Sce_FormCategory.sceneFormCategory = new Sce_FormCategory(catWidth, catHeight);
		
		return Sce_FormCategory.sceneFormCategory;
	}
	
	private Sce_FormCategory (double catWidth, double catHeight) {
		this.catWidth = catWidth;
		this.catHeight = catHeight;
		
		this.dataAgendaLang = Mix_Lang.getlang("data", "Agenda_esMx");
	}
	
	
	@Override
	public Scene buildingScene() {
		if (this.catScene == null) {
			this.compFormCategory.layout_main.getStyleClass().add("window");
			this.compFormCategory.layout_main.setPrefSize(this.catWidth, this.catHeight);
			
			this.compFormCategory.scroll_frm.setPrefWidth(this.catWidth);
			this.compFormCategory.scroll_frm.getStyleClass().add("scroll");
			this.compFormCategory.scroll_frm.setFitToWidth(true);
			
			this.makeForm();
			this.makeControls();
			
			this.compFormCategory.scroll_frm.setContent(this.compFormCategory.layout_frm);
			
			this.compFormCategory.val_noChanges.setWrapText(true);
			this.compFormCategory.val_noChanges.getStyleClass().addAll("validator", "validator--main");
			this.compFormCategory.val_noChanges.setPrefWidth(this.catWidth);
			this.compFormCategory.val_noChanges.setMinHeight(0);
			this.compFormCategory.val_noChanges.setVisible(false);
			
			this.compFormCategory.layout_main.getChildren().addAll(
					this.compFormCategory.val_noChanges,
					this.compFormCategory.scroll_frm,
					this.compFormCategory.layout_control
					);
			
			this.catScene = new Scene(this.compFormCategory.layout_main, this.catWidth, this.catHeight);
			this.catScene.getStylesheets().add(this.getClass().getResource("/styles/sce/Category_Form.css").toString());
		}
		
		return this.catScene;
	}
	
	private void makeForm () {
		this.compFormCategory.layout_frm.setSpacing(10);
		this.compFormCategory.layout_frm.getStyleClass().add("form__fields");
		this.compFormCategory.layout_frm.setPrefWidth(this.catWidth);
		
		// Label
		this.compFormCategory.frm_cat_title.setText(this.dataAgendaLang.getStringJSON("categoryTitle"));
		this.compFormCategory.frm_cat_title.getStyleClass().add("form__grid__label");
		this.compFormCategory.layout_frm.getChildren().add(this.compFormCategory.frm_cat_title);
		
		// Select
		this.compFormCategory.frm_cat_select.getStyleClass().add("form__grid__select");
		this.compFormCategory.frm_cat_select.setPrefSize(this.catWidth, this.compHeight);
		this.compFormCategory.frm_cat_select.setPromptText(this.dataAgendaLang.getStringJSON("categoryPrompt"));
		this.compFormCategory.layout_frm.getChildren().add(this.compFormCategory.frm_cat_select);
		
		ObservableList<Data_Category> categories = null;
		
		Tool_ConnectionDB conn = Tool_ConnectionDB.init();
		
		try {
			DAO_Management manaDB = DAO_Management.init(conn.getConnection());
			
			categories = manaDB.daoCategory().read();
		} catch (Exce_DAO_Exception e) {
			e.printStackTrace();
		} finally {
			conn.closeConnection();
		}
		
		if (categories.size() > 0)
			this.compFormCategory.frm_cat_select.setItems(categories);
		
		this.compFormCategory.frm_cat_select.setConverter(new StringConverter<Data_Category>() {
			
			@Override
			public String toString(Data_Category object) {
				if (object != null)
					return object.getName();
				return "";
			}
			
			@Override
			public Data_Category fromString(String string) {
				if (string != null) {
					for (Data_Category categoryItem: Sce_FormCategory.this.compFormCategory.frm_cat_select.getItems()) {
						if (categoryItem.getName().equals(string))
							return categoryItem;
					}
				}
				return null;
			}
		});
		
		// Input
		this.compFormCategory.frm_cat_text.setDisable(true);
		this.compFormCategory.frm_cat_text.setPrefSize(this.catWidth * .75, this.compHeight);
		this.compFormCategory.frm_cat_text.getStyleClass().add("form__grid__text");
		this.compFormCategory.layout_frm.getChildren().add(this.compFormCategory.frm_cat_text);
		
		this.compFormCategory.val_name.setWrapText(true);
		this.compFormCategory.val_name.getStyleClass().add("validator");
		this.compFormCategory.val_name.setPrefWidth(this.catWidth);
		this.compFormCategory.val_name.setMinHeight(0);
		this.compFormCategory.val_name.setVisible(false);
		this.compFormCategory.layout_frm.getChildren().add(this.compFormCategory.val_name);
		
	}
	
	private void makeControls () {
		this.compFormCategory.layout_control.getStyleClass().add("form__control");
		this.compFormCategory.layout_control.setPrefWidth(this.catWidth);
		
		this.compFormCategory.frm_controls.setSpacing(10);
		this.compFormCategory.frm_controls.getStyleClass().add("form__grid");
		this.compFormCategory.frm_controls.setPrefWidth(this.catWidth);
		
		this.compFormCategory.frm_btn_edit.setDisable(true);
		this.compFormCategory.frm_btn_edit.setPrefSize(this.catWidth * .25, this.compHeight * 2);
		this.compFormCategory.frm_btn_edit.setGraphic(new ImageView(Tool_Image.getImage("app", "editar", "png")));
		this.compFormCategory.frm_btn_edit.getStyleClass().addAll("form__grid__button", "form__grid__button--edit");
		this.compFormCategory.frm_controls.getChildren().add(this.compFormCategory.frm_btn_edit);

		this.compFormCategory.frm_btn_delete.setDisable(true);
		this.compFormCategory.frm_btn_delete.setPrefSize(this.catWidth * .25, this.compHeight * 2);
		this.compFormCategory.frm_btn_delete.setGraphic(new ImageView(Tool_Image.getImage("app", "basura", "png")));
		this.compFormCategory.frm_btn_delete.getStyleClass().addAll("form__grid__button", "form__grid__button--delete");
		this.compFormCategory.frm_controls.getChildren().add(this.compFormCategory.frm_btn_delete);
		
		this.compFormCategory.frm_btn_exit.setPrefSize(this.catWidth * .25, this.compHeight * 2);
		this.compFormCategory.frm_btn_exit.setGraphic(new ImageView(Tool_Image.getImage("app", "salir", "png")));
		this.compFormCategory.frm_btn_exit.getStyleClass().add("form__grid__button");
		this.compFormCategory.frm_controls.getChildren().add(this.compFormCategory.frm_btn_exit);
		
		this.compFormCategory.layout_control.getChildren().addAll(
				this.compFormCategory.frm_controls
				);
	}
	
}
