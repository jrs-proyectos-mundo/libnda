package views.category.mixins;

import java.sql.Connection;

import dao.DAO_Management;
import data.Data_Category;
import exceptions.Exce_DAO_Exception;
import javafx.collections.ObservableList;
import views.View_Category;
import views.category.Comp_FormCategory;
import views.home.mixins.Mix_Board;

/**
 *
 * Archivo Mix_FormCategory.java
 * @author Joaquin Reyes Sanchez [Hakyn Seyer] <joaquin.seyer21@gmail.com>
 * Creado el día 28/01/19 a las 11:59 PM
 * @license This software is MIT licensed (see LICENSE)
 * 
*/

public class Mix_FormCategory {
	
	public static void closeWindow () {
		View_Category winCat = View_Category.init();
		Mix_Board.enableDisableControls(false);
		winCat.getWindow().close();
	}
	
	/**
	 * 
	 * @param True para desactivar los elementos. False para activar los elementos
	 */
	public static void disabledEnableElements (boolean action) {
		Comp_FormCategory compCate = Comp_FormCategory.init();
		
		if (compCate.frm_cat_select.isDisable() != action)
			compCate.frm_cat_select.setDisable(action);
		
		if (compCate.frm_cat_text.isDisable() != action)
			if (compCate.frm_cat_select.getValue() != null)
				compCate.frm_cat_text.setDisable(action);
	}
	
	
	/**
	 * 
	 * @param onlyTextField False para analizar todos los elementos del fórmulario. True para analizar solo el textField del fórmulario
	 */
	public static int completeDataForm (boolean onlyTextField) {
		Comp_FormCategory compCate = Comp_FormCategory.init();
		
		int completeElements = 0;
		
		if (!onlyTextField) {			
			if (compCate.frm_cat_select.getValue() != null)
				completeElements++;
		}
		
		if (!compCate.frm_cat_text.getText().equals(""))
			completeElements++;
		
		return completeElements;
	}
	
	public static Data_Category catchData () {
		Comp_FormCategory compCate = Comp_FormCategory.init();
		
		Data_Category category = new Data_Category();
		
		category.setName(compCate.frm_cat_text.getText());
		
		return category;
	}
	
	/**
	 * 
	 * @param status "0" para nuevo registro. "1" para editar registro. "2" para eliminar registro
	 */
	public static void statusForm (int status) {
		Comp_FormCategory compFormCategory = Comp_FormCategory.init();
		compFormCategory.setActionForm(status);
		
		compFormCategory.frm_cat_select.setValue(null);
//		compFormCategory.frm_cat_select.setDisable(true);
		compFormCategory.frm_btn_edit.setDisable(true);
		compFormCategory.frm_btn_delete.setDisable(true);
		compFormCategory.frm_cat_text.setDisable(true);
		compFormCategory.frm_cat_text.clear();
	}
	
  public static void clearValidators () {
		Comp_FormCategory compFormCategory = Comp_FormCategory.init();
		
		compFormCategory.val_name.setText("");
		compFormCategory.val_name.setMinHeight(0);
		compFormCategory.val_name.setVisible(false);
		
		compFormCategory.val_noChanges.setText("");
		compFormCategory.val_noChanges.setMinHeight(0);
		compFormCategory.val_noChanges.setVisible(false);
  }
  
  public static void cleanForm () {
  	Comp_FormCategory compCat = Comp_FormCategory.init();
  	
  	compCat.frm_cat_text.clear();
  	compCat.frm_cat_select.setValue(null);
  }
  
  public static void chargeCategories (Connection conn) throws Exce_DAO_Exception {
  	Comp_FormCategory compFormCategory = Comp_FormCategory.init();
  	
  	ObservableList<Data_Category> listCategory = null;
  	
  	DAO_Management manaDB = DAO_Management.init(conn);
  	listCategory = manaDB.daoCategory().read();
  	
  	if (listCategory.size() > 0)
  		compFormCategory.frm_cat_select.setItems(listCategory);
  }
  
}
