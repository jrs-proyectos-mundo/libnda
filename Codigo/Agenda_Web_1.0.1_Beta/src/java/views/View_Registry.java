package views;

import javafx.stage.Stage;
import inter.Inter_Views;
import javafx.scene.Scene;

import mixins.Mix_Lang;
import tools.Tool_Image;
import tools.Tool_ReqJSON;

import views.home.mixins.Mix_Board;
import views.registry.scenes.Sce_FormAgenda;
import views.registry.listeners.List_FormAgenda;
import views.registry.mixins.Mix_FormAgenda;

/**
 *
 * Archivo View_Registry.java
 * @author Joaquin Reyes Sanchez [Hakyn Seyer] <joaquin.seyer21@gmail.com>
 * Creado el día 28/01/19 a las 12:04 AM
 * @license This software is MIT licensed (see LICENSE)
 * 
*/

public class View_Registry implements Inter_Views {

  private static View_Registry viewReg = null;

  private Stage window;

  private Tool_ReqJSON lang = null;

  private final double winWidth = 300;
  private final double winHeight = 525;

  public static View_Registry init () {
    if (View_Registry.viewReg == null)
      View_Registry.viewReg = new View_Registry();

    return View_Registry.viewReg;
  }

  private View_Registry () {
    this.window = new Stage();

    this.lang = Mix_Lang.getlang("sce", "Scene_esMx");

    if (this.lang != null) {
      this.window.setTitle(this.lang.getStringJSON("RegistryNewTitle"));
      
      this.chargeScene("");

      this.listenersWindow();

    } else System.out.println("The language did not load");
  }

  @Override
  public void chargeScene(String reqScene) {
    Scene scene = null;

    switch (reqScene) {
      default:
        Sce_FormAgenda sceneForm = Sce_FormAgenda.init(this.winWidth, this.winHeight);
        
        scene = sceneForm.buildingScene();
    }

    if (scene != null) {
      this.window.setScene(scene);
      this.window.setResizable(false);

      this.window.getIcons().add(Tool_Image.getImage("app", "aplicacion", "png"));
      
      List_FormAgenda.init();
      
    } else System.out.println("You should add a scene :(");
  }

  @Override
  public Stage getWindow() {
    return this.window;
  }

  @Override
  public void listenersWindow() {
    this.window.setOnCloseRequest(e -> {
      Mix_FormAgenda.returnWinDefaultHome(false);

      View_Home winHome = View_Home.init(null);

      Mix_Board.refreshResume(winHome.getWindow().getWidth(), winHome.getWindow().getHeight(), null);
      Mix_Board.enableDisableControls();
    });
  }

}