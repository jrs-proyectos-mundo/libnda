package dao.sql;

/**
 *
 * Archivo SQL_Category.java
 * @author Joaquin Reyes Sanchez [Hakyn Seyer] <joaquin.seyer21@gmail.com>
 * Creado el día 28/01/19 a las 11:48 PM
 * @license This software is MIT licensed (see LICENSE)
 * 
*/

public interface SQL_Category {

	public final String CREATE = "INSERT INTO category (name) VALUES (?)";
	public final String READ = "SELECT * FROM category ORDER BY name ASC";
	public final String DELETE = "DELETE FROM category WHERE id = ?";
	public final String UPDATE = "UPDATE category SET name = ? WHERE id = ?";

	public final String VAL_NAME = "SELECT * FROM category WHERE name = ?";
	public final String VAL_INUSE = "SELECT * FROM agenda_web WHERE category = ?";
}
