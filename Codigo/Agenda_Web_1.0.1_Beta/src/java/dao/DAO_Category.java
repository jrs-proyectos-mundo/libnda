package dao;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;

import org.json.simple.parser.ParseException;

import javafx.collections.ObservableList;
import mixins.Mix_Components;
import javafx.collections.FXCollections;

import data.Data_Category;
import exceptions.Exce_DAO_Exception;
import tools.Tool_ReqJSON;
import tools.Tool_Text;

import dao.base.Base_Category;
import dao.sql.SQL_Category;

/**
 *
 * Archivo DAO_Category.java
 * @author Joaquin Reyes Sanchez [Hakyn Seyer] <joaquin.seyer21@gmail.com>
 * Creado el día 28/01/19 a las 11:49 PM
 * @license This software is MIT licensed (see LICENSE)
 * 
*/

public class DAO_Category implements Base_Category {
	
	private Tool_ReqJSON lang, langComponents;
	
	private Connection conn;
	
	public DAO_Category (Connection conn) {
		this.conn = conn;
		
		try {
      this.lang = new Tool_ReqJSON("dao" , "Exceptions_esMx");
      this.langComponents = new Tool_ReqJSON("comp", "Notification_esMx");
    } catch (IOException e) {
      e.printStackTrace();
    } catch (ParseException e) {
      e.printStackTrace();
    }
	}
	
	@Override
	public boolean create(Data_Category category) throws Exce_DAO_Exception {
		PreparedStatement ps = null;
		
		try {
			ps = this.conn.prepareStatement(SQL_Category.CREATE);	
			ps.setString(1, category.getName());
			
			ps.executeUpdate();
		} catch (SQLException e) {
			throw new Exce_DAO_Exception(Tool_Text.findAndReplaceAll("##", "Crear Categoría", this.lang.getStringJSON("sqlError")), e);
		} finally {
			try {
				if (ps != null)
					ps.close();				
			} catch (SQLException e2) {
				throw new Exce_DAO_Exception(Tool_Text.findAndReplaceAll("##", "Crear Cierre", this.lang.getStringJSON("sqlClose")), e2);
			}
		}
		
		try {
			if (ps.isClosed()) return true;
			return false;
		} catch (SQLException e) {
			throw new Exce_DAO_Exception(Tool_Text.findAndReplaceAll("##", "Crear Return", this.lang.getStringJSON("sqlReturn")), e);
		}
	}



	@Override
	public ObservableList<Data_Category> read() throws Exce_DAO_Exception {
		ObservableList<Data_Category> categories = FXCollections.observableArrayList();

		Statement st = null;
		ResultSet rs = null;
		
		try {
			st = this.conn.createStatement();
			rs = st.executeQuery(SQL_Category.READ);
			
			while (rs.next()) {
				categories.add(new Data_Category(
						rs.getInt("id"), 
						rs.getString("name")
						));
			}
					
		} catch (SQLException e) {
			throw new Exce_DAO_Exception(Tool_Text.findAndReplaceAll("##", "Leer Categoría", this.lang.getStringJSON("sqlError")), e);
		} finally {
			try {
				if (st != null)
					st.close();
				
				if (rs != null)
					rs.close();				
			} catch (Exception e2) {
				throw new Exce_DAO_Exception(Tool_Text.findAndReplaceAll("##", "Leer Cierre", this.lang.getStringJSON("sqlClose")), e2);
			}
		}
		
		return categories;
	}



	@Override
	public boolean update(Data_Category category) throws Exce_DAO_Exception {
		PreparedStatement ps = null;
		
		try {
			ps = this.conn.prepareStatement(SQL_Category.UPDATE);
			ps.setString(1, category.getName());
			ps.setInt(2, category.getId());
			
			if (ps.executeUpdate() == 0)
				throw new Exce_DAO_Exception(Tool_Text.findAndReplaceAll("##", "Update Agenda", this.lang.getStringJSON("sqlNoUpdated")));
			
			Mix_Components.notificationComponent(
					"normal", 
					this.langComponents.getStringJSON("updateCategoryTitle"),
					Tool_Text.findAndReplaceAll("##", category.getName(), this.langComponents.getStringJSON("categoryContentName")),
					0
			);
			
		} catch (SQLException e) {
			throw new Exce_DAO_Exception(Tool_Text.findAndReplaceAll("##", "Update", this.lang.getStringJSON("sqlError")), e);
		} finally {
			try {
				if (ps != null)
					ps.close();
			} catch (SQLException e2) {
				throw new Exce_DAO_Exception(Tool_Text.findAndReplaceAll("##", "Update Cierre", this.lang.getStringJSON("sqlClose")), e2);
			}
		}
		
		try {
			if (ps.isClosed()) return true;
		} catch (SQLException e) {
			throw new Exce_DAO_Exception(Tool_Text.findAndReplaceAll("##", "Update Return", this.lang.getStringJSON("sqlReturn")), e);
		}
		
		return false;
	}



	@Override
	public boolean delete(Data_Category category) throws Exce_DAO_Exception {
		PreparedStatement ps = null;
		
		try {
			ps = this.conn.prepareStatement(SQL_Category.DELETE);
			
			ps.setInt(1, category.getId());
			
			if (ps.executeUpdate() == 0)
				throw new Exce_DAO_Exception(Tool_Text.findAndReplaceAll("##", "Eliminar Categoría", this.lang.getStringJSON("sqlNoDeleted")));
			
				Mix_Components.notificationComponent(
						"normal", 
						this.langComponents.getStringJSON("deleteCategoryTitle"),
						Tool_Text.findAndReplaceAll("##", category.getName(), this.langComponents.getStringJSON("categoryContentName")),
						0
				);
		} catch (SQLException e) {
			throw new Exce_DAO_Exception(Tool_Text.findAndReplaceAll("##", "Eliminar", this.lang.getStringJSON("sqlError")), e);
		} finally {
			try {
				if (ps != null)
					ps.close();
			} catch (Exception e2) {
				throw new Exce_DAO_Exception(Tool_Text.findAndReplaceAll("##", "Eliminar Cierre", this.lang.getStringJSON("sqlClose")), e2);
			}
		}
		
		try {
			if (ps.isClosed()) return true;
			
			return  false;
		} catch (SQLException e) {
			throw new Exce_DAO_Exception(Tool_Text.findAndReplaceAll("##", "Eliminar Return", this.lang.getStringJSON("sqlReturn")), e);
		}
	}
	
	@Override
	public boolean uniqueName(String name) throws Exce_DAO_Exception {
		PreparedStatement ps = null;
		ResultSet rs = null;
		
		try {
			
			ps = this.conn.prepareStatement(SQL_Category.VAL_NAME);
			ps.setString(1, name);
			
			rs = ps.executeQuery();
			
			if (rs.next()) return true;
			
		} catch (SQLException e) {
			throw new Exce_DAO_Exception(Tool_Text.findAndReplaceAll("##", "Validar Nombre", this.lang.getStringJSON("sqlError")), e);
		} finally {
			try {
				if (ps != null)
					ps.close();
				if (rs != null)
					rs.close();
			} catch (SQLException e2) {
				throw new Exce_DAO_Exception(Tool_Text.findAndReplaceAll("##", "Validar Nombre Cierre", this.lang.getStringJSON("sqlClose")), e2);
			}
		}
		
		return false;
	}
	
	@Override
	public boolean inUseCategory(int id) throws Exce_DAO_Exception {
		PreparedStatement ps = null;
		ResultSet rs = null;
		
		try {
			ps = this.conn.prepareStatement(SQL_Category.VAL_INUSE);
			ps.setInt(1, id);
			
			rs = ps.executeQuery();
			
			if (rs.next()) return true;
			
		} catch (SQLException e) {
			throw new Exce_DAO_Exception(Tool_Text.findAndReplaceAll("##", "Validar Unica Categoria", this.lang.getStringJSON("sqlError")), e);
		} finally {
			try {
				if (ps != null)
					ps.close();
				
				if (rs != null)
					rs.close();
			} catch (SQLException e2) {
				throw new Exce_DAO_Exception(Tool_Text.findAndReplaceAll("##", "Validar Unica Categoria Cierre", this.lang.getStringJSON("sqlClose")), e2);
			}
		}
		
		return false;
	}

	@Override
	public void setConnection(Connection conn) {
		this.conn = conn;
	}
	
}
