package dao.base;

import javafx.collections.ObservableList;
import data.Data_Agenda;
import exceptions.Exce_DAO_Exception;
import inter.Inter_CRUD_DAO;

/**
 *
 * Archivo Base_Agenda.java
 * @author Joaquin Reyes Sanchez [Hakyn Seyer] <joaquin.seyer21@gmail.com>
 * Creado el día 28/01/19 a las 11:47 PM
 * @license This software is MIT licensed (see LICENSE)
 * 
*/

public interface Base_Agenda extends Inter_CRUD_DAO<Data_Agenda> {

	public ObservableList<Data_Agenda> searchAgenda (String data) throws Exce_DAO_Exception;
	
}
