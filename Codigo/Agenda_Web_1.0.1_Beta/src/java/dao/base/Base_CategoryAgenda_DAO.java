package dao.base;

import data.Data_Category;
import exceptions.Exce_DAO_Exception;
import inter.Inter_CRUD_DAO;
import data.Data_Agenda;

/**
 *
 * Archivo Base_CategoryAgenda_DAO.java
 * @author Joaquin Reyes Sanchez [Hakyn Seyer] <joaquin.seyer21@gmail.com>
 * Creado el día 28/01/19 a las 11:47 PM
 * @license This software is MIT licensed (see LICENSE)
 * 
*/

public interface Base_CategoryAgenda_DAO extends Inter_CRUD_DAO<Data_Category> {

	public boolean transactionCreate (Data_Category category, Data_Agenda agenda) throws Exce_DAO_Exception;
	public boolean transactionUpdate (Data_Category category, Data_Agenda agenda) throws Exce_DAO_Exception;
	
}
