package mixins;

import java.io.InputStream;
import java.util.HashMap;

import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.util.JRLoader;
import net.sf.jasperreports.view.JasperViewer;
import tools.Tool_ConnectionDB;

/**
 *
 * Archivo Mix_Reports.java
 * @author Joaquin Reyes Sanchez [Hakyn Seyer] <joaquin.seyer21@gmail.com>
 * Creado el día 28/01/19 a las 11:54 PM
 * @license This software is MIT licensed (see LICENSE)
 * 
*/

public class Mix_Reports {

	/**
	 * 
	 * @param nameReport Nombre del reporte a generar
	 */
	public static void generateReport (String nameReport) {
		Tool_ConnectionDB conn = Tool_ConnectionDB.init();
		
		try {
			ClassLoader classLoader = ClassLoader.getSystemClassLoader();
			
			InputStream is = classLoader.getResourceAsStream("reports/"+ nameReport +".jasper");
			
			JasperReport report = (JasperReport) JRLoader.loadObject(is);

			HashMap<String, Object> data = new HashMap<>();
			
			data.put("imgLogo", Mix_Reports.class.getResource("/img/app/aplicacion.png").toString());
			
			JasperPrint print = JasperFillManager.fillReport(report, data, conn.getConnection());
			
			JasperViewer view = new JasperViewer(print, false);
			
			view.setTitle(Mix_Lang.getlang("comp", "Text_esMx").getStringJSON("txtReportAccountsTitle"));
			view.setVisible(true);
			view.setLocationRelativeTo(null);
			
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			conn.closeConnection();
		}
	}
	
}
