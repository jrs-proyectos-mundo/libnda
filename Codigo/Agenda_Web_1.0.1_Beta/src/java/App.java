import javafx.application.Application;
import javafx.stage.Stage;
import tools.Tool_ConnectionDB;
import views.View_Home;

/**
 *
 * Archivo App.java
 * @author Joaquin Reyes Sanchez [Hakyn Seyer] <joaquin.seyer21@gmail.com>
 * Creado el día 28/01/19 a las 11:46 PM
 * @license This software is MIT licensed (see LICENSE)
 * 
*/

public class App extends Application{

  @Override
  public void start (Stage mainStage) throws Exception {
  	Tool_ConnectionDB conn = Tool_ConnectionDB.init();
  	
  	if (conn.CheckExistsDataBase()) {
  		View_Home viewHome = View_Home.init(mainStage);
  		
  		viewHome.getWindow().show();  		
  	}

  }

  public static void main (String [] args) {
    launch(args);
  }

}