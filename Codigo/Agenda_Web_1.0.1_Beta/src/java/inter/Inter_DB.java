package inter;

/**
 *
 * Archivo Inter_DB.java
 * @author Joaquin Reyes Sanchez [Hakyn Seyer] <joaquin.seyer21@gmail.com>
 * Creado el día 28/01/19 a las 11:52 PM
 * @license This software is MIT licensed (see LICENSE)
 * 
*/

public interface Inter_DB {

	public String port = "3306";
	public String database = "agenda_hs";
	public String user = "root";
	public String password = "";
	
}
