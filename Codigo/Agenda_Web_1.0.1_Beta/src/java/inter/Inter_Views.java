package inter;

import javafx.stage.Stage;

/**
 *
 * Archivo Inter_Views.java
 * @author Joaquin Reyes Sanchez [Hakyn Seyer] <joaquin.seyer21@gmail.com>
 * Creado el día 28/01/19 a las 11:53 PM
 * @license This software is MIT licensed (see LICENSE)
 * 
*/

public interface Inter_Views {

  public void chargeScene (String reqScene);
  public void listenersWindow ();
  public Stage getWindow ();

}