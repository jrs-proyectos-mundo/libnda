package inter;

import javafx.collections.ObservableList;
import java.sql.Connection;

import exceptions.Exce_DAO_Exception;

/**
 *
 * Archivo Inter_CRUD_DAO.java
 * @author Joaquin Reyes Sanchez [Hakyn Seyer] <joaquin.seyer21@gmail.com>
 * Creado el día 28/01/19 a las 11:51 PM
 * @license This software is MIT licensed (see LICENSE)
 * 
*/

public interface Inter_CRUD_DAO<T> {
	
	public void setConnection (Connection conn);

	public boolean create (T data) throws Exce_DAO_Exception;
	public ObservableList<T> read () throws Exce_DAO_Exception;
	public boolean update (T data) throws Exce_DAO_Exception;
	public boolean delete (T data) throws Exce_DAO_Exception;
	
}
