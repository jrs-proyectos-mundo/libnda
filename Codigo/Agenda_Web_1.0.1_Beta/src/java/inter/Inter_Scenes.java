package inter;

import javafx.scene.Scene;

/**
 *
 * Archivo Inter_Scenes.java
 * @author Joaquin Reyes Sanchez [Hakyn Seyer] <joaquin.seyer21@gmail.com>
 * Creado el día 28/01/19 a las 11:52 PM
 * @license This software is MIT licensed (see LICENSE)
 * 
*/

public interface Inter_Scenes {
	
	public Scene buildingScene ();
	
}
