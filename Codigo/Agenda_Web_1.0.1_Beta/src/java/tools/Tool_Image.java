package tools;

import javafx.scene.image.Image;

/**
 *
 * Archivo Tool_Image.java
 * @author Joaquin Reyes Sanchez [Hakyn Seyer] <joaquin.seyer21@gmail.com>
 * Creado el día 28/01/19 a las 11:55 PM
 * @license This software is MIT licensed (see LICENSE)
 * 
*/

public class Tool_Image {

	
	/**
	 * 
	 * @param folder Nombre del folder dentro de la carpeta img
	 * @param title Titulo de la Imagen
	 * @param type Tipo de archivo de la imagen sin (.)
	 * @return
	 */
	public static Image getImage (String folder, String title, String type) {
		String route = Tool_Image.class.getResource("/img/" + folder + "/" + title + "." + type).toString();
		
		return new Image (route);
	}
	
}
