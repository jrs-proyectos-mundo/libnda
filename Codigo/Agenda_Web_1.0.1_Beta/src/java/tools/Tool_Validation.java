package tools;

import java.io.IOException;
import org.json.simple.parser.ParseException;

/**
 *
 * Archivo Tool_Validation.java
 * @author Joaquin Reyes Sanchez [Hakyn Seyer] <joaquin.seyer21@gmail.com>
 * Creado el día 28/01/19 a las 11:56 PM
 * @license This software is MIT licensed (see LICENSE)
 * 
*/

public abstract class Tool_Validation <T> {
	
	private Tool_ReqJSON lang;
	
	public Tool_Validation () {
		try {
      this.lang = new Tool_ReqJSON("tvdb", "Validation_esMx");
    } catch (IOException e) {
      e.printStackTrace();
    } catch (ParseException e) {
      e.printStackTrace();
    }
	}
	
	/**
	 * 
	 * @param data Dato correspondiente a validar
	 * @return El error si es que éxiste
	 */

	public String isEmptyString (String data) {
		String error = this.lang.getStringJSON("valNoEmpty");
		
		if (data == null) return error;
		if (data.equals("")) return error;
		
		return "";
	}
	
	/**
	 * 
	 * @param data Dato correspondiente a validar
	 * @param min Rango mínimo permítido
	 * @return El error si es que éxiste
	 */
	public String minString (String data, int min) {
		String error = this.lang.getStringJSON("valMin").replaceAll("#####", String.valueOf(min));
		
		if (data.length() < min) return error;
		
		return "";
	}
	
	/**
	 * 
	 * @param data Dato correspondiente a validar
	 * @param max Rango máximo permítido
	 * @return El error si es que éxiste
	 */
	public String maxString (String data, int max) {
		String error = this.lang.getStringJSON("valMax").replaceAll("#####", String.valueOf(max));
		
		if (data.length() > max) return error;
		
		return "";
	}
	
	/**
	 * 
	 * @param data Dato correspondiente a validar
	 * @param noValid Numero no permitido en la selección
	 * @return El error si es que éxiste
	 */
	public String selectEmptyInt (int data, int noValid) {
		String error = this.lang.getStringJSON("valSelectEmpty");
		
		if (data == noValid) return error;
		
		return "";
	}
	
	/**
	 * 
	 * @param original Dato a comparar
	 * @param compare Dato Original
	 * @param nameFieldNoEqual Nombre del campo del dato original
	 * @return El error si es que éxiste
	 */
	public String noEqual (String original, String compare, String nameFieldCompare) {
		String error = this.lang.getStringJSON("valNoEqual");
		
		if (compare == null && original == null) return Tool_Text.findAndReplaceAll("##", nameFieldCompare, error);
		
		if (compare == null && original != null) return "";
		
		if (compare.equals(original)) return Tool_Text.findAndReplaceAll("##", nameFieldCompare, error);
		
		return "";
	}

	/**
	 * 
	 * @param original Dato a comparar
	 * @param compare Dato Original
	 * @param nameFieldNoEqual Nombre del campo del dato original
	 * @return El error si es que éxiste
	 */
	public String noEqual (int original, int compare, String nameFieldCompare) {
		String error = this.lang.getStringJSON("valNoEqual");
		
		if (original == compare) return Tool_Text.findAndReplaceAll("##", nameFieldCompare, error);
		
		return "";
	}
	
	/**
	 * 
	 * @param original Objeto nuevo a remplazar
	 * @param compare Objeto viejo existente en la base de datos
	 * @return El error si es que éxiste
	 */
	public abstract String noEqualObject (T original, T compare);
	
	/**
	 * 
	 * @param data Dato correspondiente a validar
	 * @param min Rango mínimo permítido
	 * @param max Rango máximo permítido
	 * @return El error si es que éxiste
	 */
	public String valEmptyMinMax (String data, int min, int max) {
		String valError = "";
		
		valError = this.isEmptyString(data);
		if (!valError.equals("")) return valError;
		else {
			valError = this.minString(data, min);
			if (!valError.equals("")) return valError;
			else {
				valError = this.maxString(data, max);
				if (!valError.equals("")) return valError;
				else return "";
			}
		}
	}
	
	/**
	 * 
	 * @param data Dato correspondiente a validar
	 * @param min Rango mínimo permítido
	 * @param max Rango máximo permítido
	 * @return El error si es que éxiste
	 */
	public String valOptionalEmptyMinMax (String data, int min, int max) {
		String valError = "";
		
		valError = this.isEmptyString(data);
		if (valError.equals("")) {
			valError = this.minString(data, min);
			if (!valError.equals("")) return valError;
			else {
				valError = this.maxString(data, max);
				if (!valError.equals("")) return valError;
				else return "";
			}			
		}
		
		return "";
	}
	
	/**
	 * 
	 * @param keyJSON La llave de acceso del error
	 * @return El error en formato String
	 */
	public String getError (String keyJSON) {
		return this.lang.getStringJSON(keyJSON);
	}
	
}
