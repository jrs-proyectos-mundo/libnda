package tools;

import java.io.InputStream;

import javafx.application.Platform;
import javafx.scene.control.Control;
import views.backups.Comp_Backups;

/**
 *
 * Archivo Tool_ErrorsBackRestore.java
 * @author Joaquin Reyes Sanchez [Hakyn Seyer] <joaquin.seyer21@gmail.com>
 * Creado el día 28/01/19 a las 11:54 PM
 * @license This software is MIT licensed (see LICENSE)
 * 
*/

public class Tool_ErrorsBackRestore {
	
	public static void lauchErrorsDB (InputStream errorStream) {
		Comp_Backups compBacks = Comp_Backups.init();
		
		Platform.runLater(new Runnable() {
			
			@Override
			public void run() {
				try {
					String error = null;
					
					byte[] buffer = new byte[1024];

					int i = errorStream.read(buffer);
					
					while (i > 0) {
						error = new String(buffer, 0, i);
						
						i = errorStream.read(buffer);
					}
					
					if (error != null) {
						compBacks.val_db.setText(error);
						compBacks.val_db.setMinHeight(Control.USE_PREF_SIZE);
						compBacks.val_db.getStyleClass().clear();
						compBacks.val_db.getStyleClass().addAll("validator", "validator--main");
						compBacks.val_db.setVisible(true);		
					}
					
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
	
}
