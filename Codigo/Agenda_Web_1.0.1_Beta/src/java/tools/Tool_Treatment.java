package tools;

import java.text.Normalizer;
import org.apache.commons.text.WordUtils;

/**
 *
 * Archivo Tool_Treatment.java
 * @author Joaquin Reyes Sanchez [Hakyn Seyer] <joaquin.seyer21@gmail.com>
 * Creado el día 28/01/19 a las 11:55 PM
 * @license This software is MIT licensed (see LICENSE)
 * 
*/

public class Tool_Treatment {

  /**
   * 
   * @param data El dato a iniciar tratamiento
   * @return El nuevo dato tratado
   */
  public static String trim (String data) {
    if (data != null)
      return data.trim();
    return data;
  }

  /**
   * 
   * @param data El dato a iniciar tratamiento
   * @param type El tipo de texto a cambiar: lowercase, uppercase, capitalizeFully, normal,
   * @return El nuevo dato tratado
   */
  public static String text (String data, String type) {
    if (data != null) {
      String newData = null;
      
      switch (type) {
        case "lowercase":
          newData = data.toLowerCase();
          break;
        case "uppercase":
          newData = data.toUpperCase();
          break;
        case "capitalizeFully":
        	newData = WordUtils.capitalizeFully(data);
        	break;
        default:
          newData = data;
      }

      return newData;
    }
    
    return data;
  }

  /**
   * 
   * @param data El dato a iniciar tratamiento
   * @return El nuevo dato tratado
   */
  public static String accents (String data) {
    if (data != null) {
      String newData = null;
      
      newData = Normalizer.normalize(data, Normalizer.Form.NFD);
      newData = newData.replaceAll("[\\p{InCombiningDiacriticalMarks}]", "");

      return newData;
    }

    return data;
  }

  /**
   * 
   * @param data El dato a iniciar tratamiento
   * @param text El tipo de texto a cambiar: lowercase, uppercase, capitalizeFully
   * @return El nuevo dato tratado
   */
  public static String trimTextAccents (String data, String text) {
    if (data != null) {
      String newData = null;

      newData = Tool_Treatment.trim(data);
      newData = Tool_Treatment.text(newData, text);
      newData = Tool_Treatment.accents(newData);
    
      return newData;
    }

    return data;
  }

}
