package tools;

import java.util.regex.Pattern;
import java.util.regex.Matcher;

/**
 *
 * Archivo Tool_Text.java
 * @author Joaquin Reyes Sanchez [Hakyn Seyer] <joaquin.seyer21@gmail.com>
 * Creado el día 28/01/19 a las 11:55 PM
 * @license This software is MIT licensed (see LICENSE)
 * 
*/

public class Tool_Text {

	/**
	 * 
	 * @param pattern Caracteres a buscar y reemplazar
	 * @param replaceWith Caracteres sustitutos
	 * @param inputString Texto original
	 * @return
	 */
	public static String findAndReplaceAll(String pattern, String replaceWith, String inputString) {
	    Pattern search = Pattern.compile(pattern);
	    Matcher matcher = search.matcher(inputString);
	    
	    return matcher.replaceAll(replaceWith);
	}
	
}
