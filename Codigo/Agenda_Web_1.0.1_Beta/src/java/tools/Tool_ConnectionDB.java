package tools;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import inter.Inter_DB;

/**
 *
 * Archivo Tool_ConnectionDB.java
 * @author Joaquin Reyes Sanchez [Hakyn Seyer] <joaquin.seyer21@gmail.com>
 * Creado el día 28/01/19 a las 11:54 PM
 * @license This software is MIT licensed (see LICENSE)
 * 
*/

public class Tool_ConnectionDB implements Inter_DB {

	private static Tool_ConnectionDB database = null;
	
	private Connection conn = null;
//	private int idConn = 0;
	
	public static Tool_ConnectionDB init () {
		if (Tool_ConnectionDB.database == null)
			Tool_ConnectionDB.database = new Tool_ConnectionDB();
			
		return Tool_ConnectionDB.database;
	}
	
	private Tool_ConnectionDB () {}
	
	public Connection getConnection () {
		if (this.conn == null) {
			try {
//			Class.forName("org.mariadb.jdbc.Driver"); Opcional para versiones de Java superior al 6
				
//			this.idConn  = (int)(Math.random() * 100 + 1);
//			System.out.println("NUEVA CONEXION: " + this.idConn);
				
				this.conn = DriverManager.getConnection("jdbc:mariadb://localhost:"+ Inter_DB.port +"/"+ Inter_DB.database +"?user="+Inter_DB.user+"&password="+ Inter_DB.password);
				
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		
		return this.conn;			
	}
	
	public boolean CheckExistsDataBase () {
		try {
			Connection connCheck = DriverManager.getConnection("jdbc:mariadb://localhost:"+ Inter_DB.port, Inter_DB.user, Inter_DB.password);

			Statement st = null;
			ResultSet rs = null;
			
			st = connCheck.createStatement();
			rs = st.executeQuery("SHOW DATABASES LIKE '"+ Inter_DB.database +"'");
			
			if (rs.next()) return true;
			else {
			  Process p = null;
			  
			  if (Inter_DB.password.equals(""))
          p = Runtime.getRuntime().exec("mysql -u " + Inter_DB.user);
        else
				  p = Runtime.getRuntime().exec("mysql -u " + Inter_DB.user + " -p" + Inter_DB.password);
				
				OutputStream os = p.getOutputStream();
				InputStream is = this.getClass().getResourceAsStream("/sql/agenda_hs.txt");
				BufferedReader reader = new BufferedReader(new InputStreamReader(is));
				
				while (reader.ready()) {
					os.write(reader.read());
				}
				
				os.flush(); // para vaciar el buffer de salida
				os.close();
				
				if (p.waitFor() == 0) return true;
			}
			
		} catch (SQLException | IOException | InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		return false;
	}
	
	public void closeConnection () {
		try {
			if (this.conn != null)
				this.conn.close();
//			System.out.println("CERRANDO CONEXION " + this.idConn);
			this.conn = null;			
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
}
