package tools;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

/**
 *
 * Archivo Tool_ReqJSON.java
 * @author Joaquin Reyes Sanchez [Hakyn Seyer] <joaquin.seyer21@gmail.com>
 * Creado el día 28/01/19 a las 11:55 PM
 * @license This software is MIT licensed (see LICENSE)
 * 
*/

public class Tool_ReqJSON {

  private JSONObject dataJSON;

  /**
   * 
   * @param fileJSON Nombre del archivo JSON
   * @throws IOException
   * @throws ParseException
   */
  public Tool_ReqJSON (String namePackage, String fileJSON) throws IOException, ParseException {
    JSONParser parser = new JSONParser();
    
    InputStream is = this.getClass().getResourceAsStream("/lang/" + namePackage + "/" + fileJSON + ".json");
    
    Reader reader = new InputStreamReader(is, "UTF-8");

    Object jsonObj = parser.parse(reader);

    this.dataJSON = (JSONObject) jsonObj;
  }

  /**
   * 
   * @param keyJSON Llave de acceso al objecto json
   * @return Dato contenido en el objecto deseado
   */
  public String getStringJSON (String keyJSON) {
    return (String) this.dataJSON.get(keyJSON);
  }

}